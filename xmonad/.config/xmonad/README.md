# My [XMonad] setup

![](https://user-images.githubusercontent.com/50166980/111333769-42805600-8673-11eb-9952-002b0c970577.png)

[XMonad]: https://xmonad.org/

## Overview

  - An attempt was made to document the source code, even though this is
    just a personal configuration.

  - This config generally uses a lot of features that are only available
    in the git versions of `xmonad` and `xmonad-contrib`.

  - The Keybindings are optimized for the [Colemak Mod-DH] layout. In
    fact, there are a few extras on top of this, like having `Super`
    bound to holding down `Tab` and also to holding down `[`. Further, I
    have `CapsLock` bound to `Ctrl` when held down and `Escape` when
    tapped.  For more on this check out my [keyboard
    configuration]—build with the venerable [kmonad]!

  - Heavy usage of the EDSL in [X.U.Run] for spawning things from Emacs
    or the terminal, which I've also written about
    [here][blog:emacs-xmonad].  This results in a rather tight
    integration with especially Emacs (the [other config][cfg:emacs]
    that's way too long)

    ``` haskell
    proc $ inEditor >-> setFrameName "capture"
                    >-> eval (elispFun "slot/org-capture" <> args)
    ```

    would spawn Emacs with a frame name[^1] equal to that of `"capture"`
    and then execute the elisp function `slot/org-capture` with the
    arguments `args`. Beautiful, right? This also includes ways to spawn
    Emacs in `batch-mode`:

    ``` haskell
    do url <- getSelection
       proc $ inEmacs
          >-> withEmacsLibs [ ElpaLib "dash", ElpaLib "s", ElpaLib "arxiv-citation"
                            , Special "~/.config/emacs/private-stuff.el" ]
          >-> asBatch
          >-> eval (progn [require "arxiv-citation", "arxiv-citation" <> asString url])
    -- Approximate output (line breaks mine):
    --
    -- /usr/bin/sh -c  "emacs -L /home/slot/.config/emacs/elpa/dash-20220417.2250
    --                        -L /home/slot/.config/emacs/elpa/s-20210616.619
    --                        -L /home/slot/.config/emacs/elpa/arxiv-citation-20220510.1137
    --                        -l /home/slot/.config/emacs/private-stuff.el
    --                        --batch
    --                        --eval '(progn (require (quote arxiv-citation)
    --                                       (arxiv-citation <url-in-clipboard>)))'"
    --
    --
    ```

  - Submaps ([X.A.Submap]) are used in order to keep these keybindings
    somewhat ergonomic.

  - No bordes or gaps to waste space! Instead, non-focused windows are
    made slightly translucent with [compton]; see further down for a
    visual example. Config available
    [here](https://gitlab.com/slotThe/dotfiles/-/blob/master/compton/.config/compton/compton.conf).

  - A single [xmobar] per screen, showing more-or-less important
    information—with property-based logging to avoid all of the
    downsides of pipes! I also show all non-focused windows in the bar
    because that's helpful sometimes. The (haskell-based) configuration
    for xmobar is available
    [here](~/.dotfiles/xmobar/.config/xmobarrc/).

  - Topic-based workspaces with [X.A.TopicSpace] (I will use "topic" and
    "workspace" as synonyms); direct bindings for the first ten of them
    on `` ` `` and `1..9`. The rest is available via an [X.P.Workspace]
    prompt.

      - [X.A.TopicSpace] is also used for setting topic directories and
        spawning relevant programs to the topic when visiting it (iff it
        is empty), as well as setting the starting layout and doing
        other convenient things. For example, for Haskell-based topics I
        automatically switch to the `Hacking` layout (see below), spawn
        an instance of [GNU/Emacs], a [ghcid] session of the current
        project and a terminal in the projects directory.  I've written
        a blog post showcasing the module [here][blog:topic-space].

  - Keep track of the most recently visited workspace and do so in a
    *screen independent* way. This means that even if two screens are
    connected, `M-C-<Space>` will toggle between the two last focused
    workspaces on the currently focused screen only, not disturbing the
    history of the other screen.

  - Ergonomic promoting to the master pane with a combination of
    [X.A.DwmPromote] and [X.A.SwapPromote].

  - Quickly switching to any visible window with [X.A.EasyMotion].

  - Liberal use of scratchpads from [X.U.NamedScratchpad] in order to
    provide quick access to my email, calculator, agenda, and more.

  - Group similar functionality together behind Emacs-style prefix
    arguments with [X.A.Prefix].

  - Heavy use of the [X.Prompt] prompt (now with font fallback
    support!), including for interacting with [org-mode] ([X.P.OrgMode],
    blog post with additional information [here][blog:orgmode]),
    searching ([X.A.Search]), quickly opening certain git repositories
    ([X.A.Search]), switching workspaces ([X.P.Workspace]), interacting
    with `pass(1)` ([X.P.Pass]), going to and bringing windows
    ([X.P.Window]), reading `man` pages ([X.P.Man]).  Basically
    everything but—funnily enough—spawning programs, for which I use
    [hmenu].

  - Use of [X.H.Rescreen] for more automatic screen plug-and-play. This
    includes executing `autorandr --change`, setting a new wallpaper, as
    well as disabling composition in the case of more than one screen.

  - The most used layouts are:

    - `Hacking`: A [X.L.ResizableTile] base whose stack windows are
      magnified when there are more than three windows in total
      ([X.L.Magnifier]) and which is capped at showing a maximum of
      three windows ([X.L.LimitWindows]).  As the name suggests, it is
      mostly used for programming related things. Example:

      ![](https://user-images.githubusercontent.com/50166980/111333440-fd5c2400-8672-11eb-93a3-c6583a942597.png)

      ![](https://user-images.githubusercontent.com/50166980/111333455-01884180-8673-11eb-8f69-b946116ae0dd.png)

      These screenshots also showcase the aforementioned "transparency
      instead of borders" approach.

    -   A vanilla `TwoPane` layout from [X.L.TwoPane]; this is
        particularly nice when writing LaTeX.

    -   The `ThreeColMid` layout from [X.L.ThreeColumns], reflected
        horizontally with [X.L.Reflect] and also magnified by
        [X.L.Magnifier].  This is my preferred layout when using a
        bigger monitor.

    -   The `Full` layout, of course!

  - [Unclutter] friendly screenshotting with `import`, featuring a
    horrible hack involving `xdotool` (I don't know why this is a
    feature :)).

[^1]: I use `emacsclient` to spawn a frame instead of an entire second
      editor. Setting the frame name sets the [ICCCM instance
      name](https://tronche.com/gui/x/icccm/sec-4.html#WM_CLASS), which
      in this case is used for scratchpad querying of Emacs frames
      spawned this way.

[Colemak Mod-DH]: https://colemakmods.github.io/mod-dh/
[GNU/Emacs]: https://www.gnu.org/software/emacs/
[Unclutter]: https://github.com/Airblader/unclutter-xfixes
[X.A.DwmPromote]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-DwmPromote.html
[X.A.EasyMotion]: https://github.com/xmonad/xmonad-contrib/blob/master/XMonad/Actions/EasyMotion.hs
[X.A.Prefix]: https://xmonad.github.io/xmonad-docs/xmonad-contrib-0.16.999/XMonad-Actions-Prefix.html
[X.A.Search]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-Search.html
[X.A.Submap]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-Submap.html
[X.A.SwapPromote]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-SwapPromote.html
[X.A.TopicSpace]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-TopicSpace.html
[X.A.TopicSpace]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-TopicSpace.html
[X.H.Rescreen]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Hooks-Rescreen.html
[X.L.LimitWindows]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-LimitWindows.html
[X.L.Magnifier]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-Magnifier.html
[X.L.Magnifier]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-Magnifier.html
[X.L.Reflect]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-Reflect.html
[X.L.ResizableTile]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-ResizableTile.html
[X.L.ThreeColumns]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-ThreeColumns.html
[X.L.TwoPane]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Layout-TwoPane.html
[X.P.Man]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt-Man.html
[X.P.OrgMode]: https://github.com/xmonad/xmonad-contrib/blob/42307c2855ce374f60d260c406ad4aa92b484171/XMonad/Prompt/OrgMode.hs
[X.P.Pass]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt-Pass.html
[X.P.Window]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt-Window.html
[X.P.Workspace]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt-Workspace.html
[X.P.Workspace]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt-Workspace.html
[X.Prompt]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Prompt.html
[X.U.NamedScratchpad]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Util-NamedScratchpad.html
[X.U.Run]: https://xmonad.github.io/xmonad-docs/xmonad-contrib/XMonad-Util-Run.html
[blog:emacs-xmonad]: https://tony-zorman.com/posts/2022-05-25-calling-emacs-from-xmonad.html
[blog:orgmode]: https://tony-zorman.com/posts/orgmode-prompt/2022-08-27-xmonad-and-org-mode.html
[blog:topic-space]: https://tony-zorman.com/posts/topic-space/2022-09-11-topic-spaces.html
[cfg:emacs]: ../../../emacs/.config/emacs/README.md
[compton]: https://hackage.haskell.org/package/xmonad-contrib/docs/XMonad-Actions-Submap.html
[ghcid]: https://github.com/ndmitchell/ghcid
[hmenu]: https://gitlab.com/slotThe/hmenu
[keyboard configuration]: https://colemakmods.github.io/mod-dh/
[kmonad]: https://colemakmods.github.io/mod-dh/
[org-mode]: https://orgmode.org/
[xmobar]: https://github.com/jaor/xmobar

## Build

Build with `stack`, see e.g. the included `build` script; it takes the
location of the executable as an argument.

Do note that you will need to change `~/.stack.yaml` to point to the
relevant directories for xmonad and xmonad-contrib on your system; you
can mostly get away with just using some recent enough commit on git,
like it's done with `X11`.

## See also

You can, of course, also look around in the rest of these dotfiles;
though I reckon you won't see many too interesting things.
