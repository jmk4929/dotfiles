#!/bin/sh
#
# http://github.com/mitchweaver/bin
#
# Some modififactions by me.
#
# update /etc/hosts, blocking ads+malware+fakenews+gambling+social
# for more info see: http://github.com/StevenBlack/hosts
#

# if not running as root, restart script
if [ $(id -u) -ne 0 ] ; then
    if type doas > /dev/null ; then
        doas $0 "$@"
    else
        doas -E $0 "$@"
    fi
    exit $?
fi

curl -# -L https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-social/hosts > /tmp/hosts
if [ $? -eq 0 ] ; then

    # don't kill my reddit >:(
    sed -E 's/.*.redd(\.)?(it)?.*.//g' /tmp/hosts > /tmp/hosts.tmp
    echo '0.0.0.0 ads.reddit.com' >> /tmp/hosts.tmp
    echo '0.0.0.0 alb.reddit.com' >> /tmp/hosts.tmp
    for amazon_sucks in $(seq 8) ; do
        echo "0.0.0.0 reddit-image.s${amazon_sucks}.amazonaws.com"
    done >> /tmp/hosts.tmp

    # I sometimes check certain twitter feeds (e.g. void linux).
    sed -E 's/.*.twi(tter)?(mg)?.*.//g' /tmp/hosts.tmp  >> /tmp/hosts.tmp2

    mv -f /tmp/hosts.tmp2 /tmp/hosts
    mv -f /tmp/hosts /etc/hosts

fi
