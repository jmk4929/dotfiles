#!/bin/sh

if [ "$1" = "run" ] ; then
    shift      # $2 == $1 now.
    if [ "$1" = "only-files" ] ; then
        menu="hmenu -o --"
        shift
    else
        menu="hmenu --"
    fi
else
    menu=dmenu
fi

# Options:
#   -i: match items case insensitively.
#   -f: grab keyboard before reading stdin.
#   -[n,s][b,f]: Define [normal, selected] [background, foreground] colour.
#   -fn: Font
$menu -i -f -l 5 -nb '#24273a' -nf '#cdd6f4' -sb '#94e2d5' -sf '#000000' -fn 'iosevka custom-10' "$@"
