#!/bin/sh

# Check if internet is running.
ping -q -w 1 -c 1 "$(ip r | grep default | cut -d ' ' -f 3)" >/dev/null || exit

# Fetch mail once, in quiet mode.
offlineimap -o -u quiet

# All of my notmuch tags
"$HOME"/.local/bin/notmuch-new-tag.sh
