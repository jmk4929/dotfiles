#!/bin/sh

BUILD_OPTS=$(emacs --batch --eval "(prin1 system-configuration-options)")

./autogen.sh
echo "$BUILD_OPTS" | sed 's/^"\(.*\)"$/\1/' | xargs ./configure
make bootstrap
sudo make install
