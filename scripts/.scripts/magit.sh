#!/bin/sh

# Copyright (C) 2020  Adam Porter
#               2021  slotThe

# Author: Adam Porter <adam@alphapapa.net>
# URL: https://github.com/alphapapa/magit.sh
# This Version: https://gitlab.com/slotThe/dotfiles/-/blob/master/scripts/.scripts/magit.sh

# * Commentary:

# Run a standalone Magit editor!  To improve startup speed, this
# script ignores the user's Emacs init files and only loads the Emacs
# libraries Magit requires.

# Note that this does NOT install any packages.  By default, magit and
# its dependencies must already be installed in ~/.emacs.d.  This is
# configurable with the --config (-c) option.

# An example invocation might look like:
#
# magit.sh -x --config ~/.config/emacs/ --eval "(menu-bar-mode -1) (scroll-bar-mode -1) (tool-bar-mode -1) (load-theme 'modus-operandi)"

# * License:

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# * Code:

load_paths() {
    # Dependencies:
    set -- magit magit-section async dash with-editor git-commit transient compat

    # Echo the "-L PATH" arguments for Emacs.  Since multiple versions
    # of a package could be installed, and we want the latest one, we
    # sort them and take the top one.
    for package in "$@"
    do
        find "$config_path"/elpa -maxdepth 1 -type d -iname "$package-2*" \
            | sort -r | head -n1 | \
            while read -r path
            do
                printf -- '-L %s ' "$path" | sed "s/'/'\\\\''/g"
            done
    done
}

usage() {
    cat <<EOF
It's Magit!

magit.sh [OPTIONS] [PATH]

Options:
  -h, --help    This.
  -x, --no-x    Display in terminal instead of in a GUI window.
  -c, --config  Set the Emacs configuration path.
  -e, --eval    Evaluate the given string as elisp on startup.
EOF
}

# * Args

args=$(getopt -n "$0" -o hxc:e: -l help,no-x,config:,eval: -- "$@") || { usage; exit 1; }
eval set -- "$args"

# Default configuration path
config_path="$HOME/.config/emacs"

# Args to give to Emacs's --eval option.
eval_args="(local-set-key \"q\" #'kill-emacs)"

while [ -n "$1" ]
do
    case "$1" in
        -h|--help)
            usage
            exit
            ;;
        -x|--no-x)
            gui="-nw"
            shift
            ;;
        -c|--config)
            config_path="$2"
            shift 2
            ;;
        -e|--eval)
            eval_args="$eval_args $2"
            shift 2
            ;;
        --)
            shift
            ;;
    esac
done

# * Main

[ -d "$1" ] && (cd "$1" || exit)

emacs -q $gui \
      $(load_paths) \
      -l magit -f magit-status \
      --eval "(progn $eval_args)" \
      -f delete-other-windows
