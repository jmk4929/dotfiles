#!/bin/sh

if [ "$(hostname)" = "pbox" ]; then
    eval "$(dbus-launch --sh-syntax)"  # needs dbus-x11
    flatpak run org.signal.Signal
else
    signal-desktop
fi
