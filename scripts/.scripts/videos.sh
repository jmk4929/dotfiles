#!/bin/sh

# Video directory.
DIR=$HOME/videos/*

# Select with dmenu and open with mpv.
# 25 is the line count.
mpv "$(readlink -f $DIR | dmenu.sh -l 25)" >/dev/null 2>&1 &
