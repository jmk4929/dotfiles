#!/usr/bin/env bb

;; Script to open a specific link with an appropriate program.
;; Basically a (more or less) light-weight xdg-open replacement.

(ns decide-link
  (:require [clojure.string :as str]
            [babashka.process :refer [shell sh]]
            [babashka.fs :as fs]))

(defn download [thing]
  (let [path (str "/tmp/" (str/replace thing "/" ""))]
    (shell "curl -o" path "-sL" thing)
    path))

(defn image-viewer [img]     (shell "sxiv -abq" img))
(defn audio-player [aud]     (shell "mpv --no-video" aud))
(defn pdf-viewer   [pdf]     (shell "zathura" pdf))
(defn editor       [file]    (shell "emacsclient -a '' -c" file))
(defn unpacker     [archive] (shell "aunpack" archive))
(defn office       [file]    (shell "libreoffice" file))
(defn video-player [vid]     (shell "mpv --script-opts=ytdl_hook-ytdl_path=yt-dlp --ytdl-format=\"bv*[height<=1080]+ba/b\""
                                    "--loop" "--hwdec=vaapi" vid))

(defn decide [arg]
  (letfn [(mime-find [re mime] (and mime (re-find re mime)))
          (file-find [re]                (re-find re arg))]
    (let [fparg (str (fs/expand-home arg))
          mime (let [mime (:out (sh "file --brief --mime" fparg))]
                 ;; 'file' returns exit code 0 even when it could not
                 ;; find anything 🙃
                 (when-not (re-find #"^cannot open" mime)
                   mime))
          proc (cond
                 ;; local
                 (mime-find #"image/vnd.djvu|application/pdf|application/postscript" mime) (pdf-viewer fparg)
                 (mime-find #"image" mime) (image-viewer fparg)
                 (mime-find #"video" mime) (video-player fparg)
                 (mime-find #"audio" mime) (audio-player fparg)
                 (mime-find #"text|inode/symlink|application/csv" mime) (editor fparg)
                 (mime-find #"application/vnd.oasis.opendocument.spreadsheet" mime) (office fparg)
                 (file-find #"xopp$") (shell "xournalpp" fparg)
                 (file-find #"tar$|gz$|xz$|bz2$|zip$|7z$") (unpacker fparg)
                 (file-find #"mp3$|flac$|ogg$|opus$") (audio-player fparg)
                 ;; remote
                 (file-find #"v.redd.it|dailymotion.com|streamable.com|hooktube.com|youtu.be|youtube.com|twitch.tv|tenor.com|.mkv$|.mp4$|.webm$|.gifv$") (video-player arg)
                 (file-find #"jpg$|JPG$|jpg:large$|jpg:orig$|format=jpg\&name=|jpeg$|JPEG$|gif$|GIF$|png$|PNG$|format=png\&name=|png:large$|png:orig$") (image-viewer (download arg))
                 (file-find #"txt$") (editor (download arg))
                 (file-find #"pdf$") (pdf-viewer (download arg))
                 :else (shell "browser-new-window.sh" arg))
          ]
      (when (not= 0 (:exit proc))
        (:err proc)))))

(run! decide *command-line-args*)
