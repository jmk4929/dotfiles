#!/bin/bash
#
# Chroot into a specified directory.
#

# If no argument is supplied, error out
if [ ! $1 ]; then
  echo "Usage: startchroot CHROOT";
  exit;
fi

# If directory doesn't exist, error out
CHROOT=$1
if [ ! -d $HOME/chroot/$1 ]; then
  echo "Couldn't find chroot"
  exit 1
fi

# Forward Xauthority for X access
doas rm $HOME/chroot/$CHROOT/root/.Xauthority
doas cp $HOME/.Xauthority $HOME/chroot/$CHROOT/root/

# Forward resolv.conf for internet access
doas rm $HOME/chroot/$CHROOT/etc/resolv.conf
doas cp /etc/resolv.conf $HOME/chroot/$CHROOT/etc/resolv.conf

# Remount necessary directories inside the chroot
if [[ $(mount | grep $CHROOT) = "" ]]; then
  xhost +si:localuser:root ### This is so we can make some apps use X11
  doas mount -o bind /dev $HOME/chroot/$CHROOT/dev
  doas mount -o bind /dev/pts $HOME/chroot/$CHROOT/dev/pts
  doas mount -o bind /proc $HOME/chroot/$CHROOT/proc
  doas mount -o bind /sys $HOME/chroot/$CHROOT/sys
  doas mount -o bind /run $HOME/chroot/$CHROOT/run
fi

# Actually chroot into the system, using bash
doas chroot $HOME/chroot/$CHROOT/ /bin/bash

# When done, unmount the directories
echo "Cleaning up..."
doas umount $HOME/chroot/$CHROOT/dev/pts
doas umount $HOME/chroot/$CHROOT/dev
doas umount $HOME/chroot/$CHROOT/proc
doas umount $HOME/chroot/$CHROOT/sys
doas umount $HOME/chroot/$CHROOT/run
