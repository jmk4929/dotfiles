#!/bin/sh

DIR=$HOME/videos

xclip -o | xargs youtube-dl --output "$DIR"/'%(uploader)s - %(title)s.%(ext)s' \
                            --add-metadata                                     \
                            --no-mark-watched                                  \
                            --force-ipv4                                       \
                            --ignore-errors                                    \
                            &
