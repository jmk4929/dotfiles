#!/bin/sh

## Directory where the config files are located.
DIR=openvpn

## Get random VPN server.
VPN=$(find "$HOME"/"$DIR"/*.conf | shuf -n 1)

## Print which server was chosen.
echo "$VPN"

## Connect to server.
cd "$HOME"/"$DIR" && doas openvpn "$VPN"
