## Execute .profile.
emulate sh -c '. ~/.profile'

## Start the X session.
if [[ ! $DISPLAY ]]; then
  exec launchx >& ~/.xsession.log
fi
