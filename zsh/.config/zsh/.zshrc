################################################################################
# Initial configuration
################################################################################

setopt auto_cd        # cd into a dir by just writing the dir name.
setopt extended_glob  # Extended globbing, awesome!

unsetopt beep         # No beep.
bindkey -e            # Emacs bindings.

################################################################################
# History
################################################################################

HISTFILE=~/.config/zsh/zsh_history  # Set histfile.
HISTSIZE=100_000   # Size of interactive history.
SAVEHIST=100_000   # Size of history in histfile.

setopt hist_ignore_dups    # Ignore duplicate in history.
setopt share_history       # Share history between all sessions.
setopt hist_reduce_blanks  # Remove superfluous blanks before recording entry.
setopt inc_append_history  # Immediately append to hist file.
setopt append_history      # Append history to the hist file (no overwriting).

# HIST_IGNORE_ALL_DUPS will throw out all previous matches of the command.
# This might be confusing when using the history as a log of what you did
# earlier.  I usually don't use my history this way, hence this gets enabled.
setopt hist_ignore_all_dups

# Emacs-like bindings for history substring search.
bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down

################################################################################
# Source local files
################################################################################

# Private aliases.
# NOTE: Not in the dotfiles repo due to it containing some sensitive data.
source ~/.config/zsh/aliases-sensitive.zsh

# Normal aliases.  The aliases file sets some aliases to auto expand, hence it
# is loaded quite early so everything specified after the loading of this file
# will *not* auto expand.
source ~/.config/zsh/aliases.zsh

# Completion settings.
autoload -U compinit
source ~/.config/zsh/completion.zsh

# Automagically escape URLs.
autoload -Uz url-quote-magic
autoload -Uz bracketed-paste-magic
zle -N self-insert url-quote-magic
zle -N bracketed-paste bracketed-paste-magic

################################################################################
# Load plugins
################################################################################

# Z for faster directory switching.
# Default alias: z
export _Z_DATA=~/.config/z  # Move history directory to .config.
source "/home/slot/.config/zsh/plugins/z/z.sh"

# like normal z when used with arguments but displays an fzf prompt when used
# without.  Alias it to 'n' for better reach.
# Source: https://github.com/junegunn/fzf/wiki/examples
unalias z
n() {
  [ $# -gt 0 ] && _z "$*" && return
  cd "$( _z -l 2>&1                                                  \
       | fzf --height 40% --nth 2.. --reverse --inline-info +s --tac \
             --query "${*##-* }"                                     \
       | sed 's/^[0-9,.]* *//'                                       \
       )"
}

# Fish-like autosuggestions.
source "/home/slot/.config/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
# Set color of autosuggestions.
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#999999"

# Completions for zsh.
source "/home/slot/.config/zsh/plugins/zsh-completions/zsh-completions.plugin.zsh"

# Fish-like history substring search.
source "/home/slot/.config/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh"

# Fancy syntax highlighting.
source "/home/slot/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
compinit

if [[ "$INSIDE_EMACS" == *comint* ]]; then
    PS1='%F{magenta}%~%f %# '
else
    # Prompt.
    source "/home/slot/.config/zsh/plugins/pure/async.zsh"
    source "/home/slot/.config/zsh/plugins/pure/pure.zsh"
fi

################################################################################
# Other Custom Configuration
################################################################################

# Automatically list directory contents on `cd`.
# Note that due to me using exa instead of ls this hast to be loaded *after* the
# sourcing of the aliases file.
chpwd() ls

# Edit the current state of the command line in the system's $EDITOR.
autoload edit-command-line; zle -N edit-command-line
bindkey '^V' edit-command-line

# Proper killing of words with C-w.
# Let "|" denote the cursor position.  Given an expression of the form
# "path/to/something|", pressing C-w gets us "path/to/" instead of completely
# deleting everything.
# I chose this solution instead of redefining `WORDCHARS` globally so jumping
# over words with M-f and M-b still works smoothly.
backward-kill-dir () {
    # Default WORDCHARS: *?_-.[]~=/&;!#$%^(){}<>
    local WORDCHARS=${WORDCHARS/\/}
    zle backward-kill-word
}
zle -N backward-kill-dir
bindkey '^W' backward-kill-dir
