#########################################################################
# Functions
#########################################################################

# Open a thing and dismiss the window.  This is more convenient
# sometimes.
function o() { decide-link.sh "$1" & disown ; exit }

# This can sometimes be useful when `decide-link' would open it in a
# browser etc.
function em() { emacsclient -a '' -c "$1" & disown }

# Create a directory and then cd into it.
# Source: grb dotfiles.
function mkcd() { mkdir -p $1 && cd $1 }

# fh - repeat history ([f]zf [h]istory)
# Adapted from: https://github.com/junegunn/fzf/wiki/examples
# Added: Don't display non-unique results.
fh() {
    print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) \
              | awk '{ $1 = " "; print }'                  \
              | awk '!($0 in a){ a[$0];print }'            \
              | fzf --no-sort --tac                        \
              | sed -r 's/ *[0-9]*\*? *//'                 \
              | sed -r 's/\\/\\\\/g'                       \
              )
}

# Just type '...' to get '../..'
# This also works for higher order dots, e.g. '....' will get you '../../..'
rationalise-dot() {
    local MATCH
    if [[ $LBUFFER =~ '(^|/| |	|'$'\n''|\||;|&)\.\.$' ]]; then
        LBUFFER+=/
        zle self-insert
        zle self-insert
    else
        zle self-insert
    fi
}
zle -N rationalise-dot
bindkey . rationalise-dot
## without this, typing a . aborts incremental history search
bindkey -M isearch . self-insert

#########################################################################
# Aliases
# Also make most things expand on space (see below).
#########################################################################

# Xbps things.
alias xi="doas xbps-install -S"
alias xr="doas xbps-remove -R"
alias xs="xbps-query -Rs"
alias update="topgrade --no-retry; doas xdiff; xcheckrestart"

# Maintenance.
alias fw="doas sv restart NetworkManager"
alias syslog="svlogtail"

# Aliases for programs.
alias d="decide-link.sh"
alias s="sxiv"
alias ssh="TERM=xterm-256color ssh"
alias top="htop"
alias v="nvim"
alias vim="nvim"
alias mpno="mpv --video=no"
alias mpl="mpv --ytdl-raw-options=yes-playlist="
alias mpln="mpv --video=no --ytdl-raw-options=yes-playlist="
alias z="zathura"
alias m="magit.sh -x --config ~/.config/emacs/ --eval \"(menu-bar-mode -1) (scroll-bar-mode -1) (tool-bar-mode -1) (visual-line-mode) (load-theme 'modus-operandi)\""
alias xpp="xournalpp"

# KMonad
alias restartkbd="pkill -9 kmonad ; kmonad -linfo ~/.config/kmonad/x220-slot-us-colemak-dh-z.kbd &"
alias kbdinternal="pkill -9 kmonad ; kmonad --input=\"device-file '/dev/input/by-path/platform-i8042-serio-0-event-kbd'\" -linfo ~/.config/kmonad/x220-slot-us-colemak-dh-z.kbd &"
alias kbdexternal="pkill -9 kmonad ; kmonad --input=\"device-file '/dev/input/by-id/usb-04d9_USB_Keyboard-event-kbd'\" -linfo ~/.config/kmonad/x220-slot-us-colemak-dh-z.kbd &"

# Stack
alias hdk="stack haddock --no-haddock-deps"
alias si="stack install"
alias sb="stack build"

# *Nice* way to change alacritty colors on the fly (live-reloading
# *configs \o/).
alias dark="sed -i 's/colors: \*gls/colors: \*gd/' ~/.config/alacritty/alacritty.yml"
alias light="sed -i 's/colors: \*gd/colors: \*gls/' ~/.config/alacritty/alacritty.yml"

# Make plugins autoexpand on space.
# https://github.com/jarmo/expand-aliases-oh-my-zsh/blob/master/expand-aliases.plugin.zsh
typeset -a ealiases
ealiases=(`alias | sed -e 's/=.*//'`)

_expand-ealias() {
  if [[ $LBUFFER =~ "(^|[;|&])\s*(${(j:|:)ealiases})\$" ]]; then
    zle _expand_alias
    zle expand-word
  fi
  zle magic-space
}
zle -N _expand-ealias

bindkey ' '  _expand-ealias         # Normal space to trigger alias expansion.
bindkey '^ ' magic-space            # Control space to bypass completion.
bindkey -M isearch " " magic-space  # Normal space during searches.

#
# NOTE: Aliases placed here will *not* be autoexpanded.  This is not an
#       optimal solution but it's the best I have for now.

# Make rust play nice with musl
alias cargo="RUSTFLAGS=\"-C target-feature=-crt-static\" cargo"

# Be safe, wear a condom.
alias rm="rm -i"
alias cp="cp -i"

# Sometimes you don't want to be annoyed by confirmation prompts.
alias rmf="rm -rf"

# Use exa, an ls replacement written in rust, for all things ls.
alias ls="exa --group-directories-first"
alias l="ls -a"      # Quick ls.
alias la="exa -laF"  # long list, show almost all, show type, human readable.
alias ll="ls -lF"    # size, show type, human readable.

# Default [e]ditor.
alias e="$(echo $EDITOR)"

# arbtt-stats, but only show the current day.
alias arbtt-today="arbtt-stats --filter='\$date>='`date +\"%Y-%m-%d\"`"
alias arbtt-yest="arbtt-stats --filter='\$date>='`date -d yesterday +\"%Y-%m-%d\"`"
