" ---> Mail specific commands <---

" Wrap text at this many characters.
" (-1) for the space at the end due to f=f
setl textwidth=71

" Needed for f=f.
" q: Allow formatting of comments with "gq".
" w: Trailing white space indicates a paragraph continues in the next line.
" a: Every time text is inserted or deleted the paragraph will be reformatted.
setl formatoptions+=awq

" Comment settings for email.
" n: Nested comment.
" b: Blank (<Space>, <Tab> or <EOL>) required after {string}.
" >: character with which comments start.
setl comments+=nb:>
