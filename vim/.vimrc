"""
""" __   _(_)_ __ ___  _ __ ___
""" \ \ / / | '_ ` _ \| '__/ __|
"""  \ V /| | | | | | | | | (__
"""   \_/ |_|_| |_| |_|_|  \___|
"""
""" For language specific settings see ~/.vim/ftplugin/*language*
"""

""" No backups because I like to live on the edge :>
""" Also disable vi compatible mode to take full advantage of vim's features
"""
set nocompatible
set nobackup

""" Vimplug
"""
" Install if not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugins
call plug#begin('~/.vim/plugged')
  " If tpope didn't exist, it would be necessary to create him.
  Plug 'tpope/vim-sensible'                                "Sensible defaults
  Plug 'tpope/vim-surround'                                "ez surround change
  Plug 'tpope/vim-commentary'                              "ez comments
  Plug 'dracula/vim', { 'as': 'dracula' }                  "Dracula colour scheme
  Plug 'morhetz/gruvbox'                                   "Gruvbox colour scheme
  Plug 'jnurmine/Zenburn'                                  "Zenburn colour scheme
  Plug 'SirVer/ultisnips'                                  "Handling snippets
  Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }   "LaTeX
  " Plug 'Valloric/YouCompleteMe', { 'do': './install.py' }  "Autocomplete
  Plug '/usr/local/opt/fzf'                                "fzf
  Plug 'junegunn/fzf.vim'                                  "fzf for vim
  Plug 'w0rp/ale'                                          "code linting
  Plug 'junegunn/goyo.vim'                                 "distraction free writing
call plug#end()

""" Goyo settings
"""
let g:goyo_width=84

""" LaTeX settings
"""
let g:livepreview_engine = 'pdflatex -pdf'
let g:livepreview_previewer = 'zathura'

""" UltiSnips configuration
"""
" Set runtime path
set rtp+=~/.vim

" Where to store snippets
let g:UltiSnipsSnippetsDir = "~/.vim/UltiSnips"
let g:UltiSnipsSnippetDirectories=["UltiSnips"]
let g:UltiSnipsEditSplit= "context"

" Trigger configuration. Can't use <tab> because I use YouCompleteMe.
" Most of my often used snippets expand automatically anyway so this is not
" *that* important.
let g:UltiSnipsExpandTrigger='<C-s>'
let g:UltiSnipsJumpForwardTrigger='<C-f>'
let g:UltiSnipsJumpBackwardTrigger='<C-b>'

""" General settings
"""
" Default max text width
set tw=79 " 80, 120, 132, 65, 84, etc.

" In insert mode, bind <C-d> to _delet_ spelling mistakes
" I only use this occasionally, so nospell is the default
set nospell
set spelllang=en_gb,de
inoremap <C-d> <C-g>u<Esc>[s1z=`]a<C-g>u

" Set the status line
" It's either this or vim-airline
" set statusline=%f%m%r%h%w\ [%Y]\ [0x%02.2B]%<\ %F%=%4v,%4l\ %3p%%\ of\ %L
" set statusline=[%n]\ %<%.99f\ [0x%02.2B]\ %y%h%w%m%r%=%-14.(%l,%c%V%)\ %P
" set statusline=[%n]\ %<%.99t\ [0x%02.2B]\ %y%h%w%m%r%=%-14.(%l,%c%V%)\ %P
set statusline=[%n]\ %<%.99f\ %y%h%w%m%r%=%-14.(%l,%c%V%)\ %P

" Default tab settings
set tabstop=2
set shiftwidth=2
set expandtab    " Make tabs behave as spaces

" Row numbers + relative numbers
set number
set relativenumber

" Syntax highlighting only works on lines with <= 500 characters.  Really I
" should be able to set this to 79 but some people are weird.
set synmaxcol=500

" Faster redraw and only redraw when necessary
set ttyfast
set lazyredraw

" Highlight matched terms when searching with "/" or "?"
" <C-l> to clear as defined in vim-sensible
set hlsearch

" Enable Goyo by default in neomutt.
augroup muttgoyostart
  autocmd!
  au BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=72
  au BufRead,BufNewFile /tmp/neomutt* :Goyo
augroup END

""" Colors
"""
" set Vim-specific sequences for RGB colours
" Basically makes termguicolors work in st/tmux
" https://github.com/vim/vim/issues/993#issuecomment-255651605
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Colour scheme
set t_Co=256
set termguicolors
colorscheme zenburn
" colorscheme dracula
" set background=light
" let g:gruvbox_italic='1'
" let g:gruvbox_contrast_light="hard"
" colorscheme gruvbox

""" Trailing white spaces
"""
" Show trailing white spaces/space-tab mixes
" Colour is Dracula's purple
highlight ExtraWhitespace ctermbg=red guibg=#BD93F9
augroup trailingwhitespace
  " Clear
  autocmd!
  " Don't show trailing spaces on current line while in insert mode
  au InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
  " Show them as soon as you leave insert mode
  au InsertLeave * match ExtraWhitespace /\s\+$/
augroup END

" Start with Normal Mode settings
match ExtraWhitespace /\s\+$/
