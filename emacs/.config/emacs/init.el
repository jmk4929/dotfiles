;;; -*- lexical-binding: t; -*-

;;; Commentary

;; Tested and working with Emacs Git:
;;
;; + `system-configuration-options': "--with-x --without-x-toolkit
;; --without-toolkit-scroll-bars -without-dbus --without-gconf
;; --without-gsettings --with-modules -with-file-notification=inotify
;; --with-jpeg --with-tiff --with-gif -with-png --with-xpm --with-rsvg
;; --without-imagemagick --with-cairo -with-gnutls --with-sound
;; --with-json --with-harfbuzz --with-gpm -with-native-compilation
;; --without-compress-install --with-xinput2 -with-small-ja-dic
;; 'CFLAGS=-O2 -pipe -march=native -mtune=native fomit-frame-pointer'"
;;
;; I'm using my own spin of colemak-ansi-dh (or whatever its name is) as
;; my keyboard layout, so your mileage on the binds may vary, though
;; most of them concentrate on mnemonics anyways.
;;
;; External things needed for this config to be plug and play (it still
;; won't be):
;;
;;   - Basic development tools (git, ghc, stack, leiningen,
;;     cargo... basically anything that you might use if you program in
;;     Haskell, Clojure, Rust, whatever else might be in here)
;;   - LaTeX packages, zathura
;;   - iosevka custom (https://gitlab.com/slotThe/iosevka-solid)
;;   - rg, fzf (used by `consult')
;;   - aspell (used by `flyspell')
;;   - Pandoc (used by `markdown-mode')
;;   - xclip
;;   - delta (used by `magit' for nicer diffs)
;;   - proselint (used by `flycheck')
;;
;; Don't forget to change the paths (mostly starting with `~/repos') to
;; something present on your system.
;;
;; Sources of great inspiration have been:
;;   - https://github.com/hrs/dotfiles
;;   - https://github.com/joedicastro/dotfiles
;;   - https://github.com/munen/emacs.d
;;   - https://github.com/LinFelix/dotEmacs
;;   - https://github.com/belak/dotfiles
;;   - https://zge.us.to/emacs.d.html
;;   - https://leahneukirchen.org/dotfiles/.emacs

;;; Code

;; My elisp files.
;; 24dec2019 ho-ho-ho
(add-to-list 'load-path (concat user-emacs-directory "lisp/"))

;; Set custom file :>
(setq custom-file (make-temp-file "emacs-custom.el_"))

(set-language-environment "UTF-8")
(setq default-input-method nil)         ; This gets set by the above—dont'.

(setq vc-follow-symlinks t   ; Always follow symlinks without prompting.
      default-directory  "~/")   ; Default directory for current buffer.

;;;; Packages

;; Set up package repositories.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") 'append)
(setq package-native-compile t
      native-comp-async-report-warnings-errors nil)
;; Initial `use-package' configuration.
(require 'use-package)
(setq use-package-always-ensure t       ; :ensure t  by default
      use-package-always-defer  t)      ; :defer  t  by default
(unless (package-installed-p 'vc-use-package)
  (package-vc-install "https://github.com/slotThe/vc-use-package"))
(use-package dash :demand)
(require 'vc-use-package)

;;;; Garbage Collector Things

(use-package gcmh                       ; 28mar2022
  :demand t
  :config (gcmh-mode)
  :custom (gcmh-high-cons-threshold (* 16 1024 1024)))

;;; Include Local Files
;; 06sep2021 delete evil mode Oo let's see how
;;           long it takes until I regret this :]

(use-package s :demand)

(require 'sensible-defaults)

(require 'startup-screen)
(require 'theming)
(require 'keybindings)

(require 'completion)
(require 'window-management)
(require 'dired-config)
(require 'global-packages)
(require 'parens-config)

(require 'writing)
(require 'email)
(require 'rss)
(require 'erc-config)

(require 'programming)
(require 'eshell-config)
(require 'latex-math)
(require 'latex-config)
(require 'org-setup)

;; Private things included specific directory names etc. I don't want to
;; publish (nothing too interesting anyways).
(load (concat user-emacs-directory "private-stuff.el"))

(setq ediff-window-setup-function 'ediff-setup-windows-plain)
