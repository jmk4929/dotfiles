;;; -*- lexical-binding: t; -*-

;; Emacs27 introduces `early-init.el', run before `init.el'.  Basically
;; settings you want to set before package and UI initialization
;; happens; like disabling all that shit :)
;; 24dec2019 ho-ho-ho

;; Use a rather large GC threshold on startup, but revert back to
;; sensible settings afterwards.
;;
;; Stolen from: https://github.com/jwiegley/dot-emacs/blob/814345f/init.el
;; 31aug2021
(defvar file-name-handler-alist-old file-name-handler-alist)
(setq file-name-handler-alist   nil
      message-log-max           16384
      gc-cons-threshold         402653184
      gc-cons-percentage        0.6)
(add-hook 'after-init-hook
          (lambda ()
            (setq file-name-handler-alist file-name-handler-alist-old
                  gc-cons-threshold 16777216
                  gc-cons-percentage 0.1)
            (garbage-collect))
          t)

;; Always recompile libraries if needed.  This being in early init is in
;; line with `auto-compile's manual.
(let* ((dir "~/.config/emacs/elpa/")
       (packages (directory-files dir))
       (get-pkg (lambda (pkg)
                  (seq-find (lambda (p) (string-prefix-p pkg p))
                            packages))))
  (dolist (pkg (list (funcall get-pkg "compat")
                     (funcall get-pkg "packed")
                     (funcall get-pkg "auto-compile")))
    (add-to-list 'load-path (concat dir pkg))))
(setq load-prefer-newer t)
(unless (package-installed-p 'auto-compile)
  (package-refresh-contents)
  (package-install 'auto-compile))
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)

;; 20sep2022  https://github.com/jimeh/.emacs.d/blob/06bf89af61413aaf610e7a293306c01e330f3302/early-init.el#L54
;;
;; Don't resize the frame to preserve the number of columns or lines
;; being displayed when setting font, menu bar, tool bar, tab bar,
;; internal borders, fringes, or scroll bars.  Since I use XMonad, this
;; option is i) useless anyways and ii) _terribly_ expensive.
(setq frame-inhibit-implied-resize t)

;; Emacs has many "visual benefits" that aim to make it intuitive for
;; some Generation Z people...  I think?
(menu-bar-mode   -1)
(scroll-bar-mode -1)
(tool-bar-mode   -1)
