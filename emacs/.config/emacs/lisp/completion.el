;;; completion.el --- My completion setup -*- lexical-binding: t; -*-

;;; The cool new completion framework on the block is several individual
;;; packages!  Apparently modularity is nice, who knew.
;; 26oct2021

;;; `vertico'
(use-package vertico
  :demand t
  :config (vertico-mode)
  :preface
  (defun slot/mini-down ()
    "Move to next candidate in minibuffer.
This works even when the minibuffer isn't selected."
    (interactive)
    (with-selected-window (active-minibuffer-window)
      (execute-kbd-macro [down])))

  (defun slot/mini-up ()
    "Move to previous candidate in minibuffer.
This works even when the minibuffer isn't selected."
    (interactive)
    (with-selected-window (active-minibuffer-window)
      (execute-kbd-macro [up])))
  :bind (("C-x C-m" . execute-extended-command)
         ("C-x f"   . find-file)
         ("C-c n"   . slot/mini-down)
         ("C-c p"   . slot/mini-up))
  :config
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
  :custom
  (vertico-cycle t)
  (enable-recursive-minibuffers t) ; Minibuffers inside minibuffers!
  ;; Hide M-x commands that do not work in current mode
  (read-extended-command-predicate #'command-completion-default-include-p))

(use-package vertico-directory          ; 26mar2022
  :after vertico
  :ensure nil
  :bind (:map vertico-map
              ("DEL" . vertico-directory-delete-char)
              ("C-w" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

;;; `orderless'
(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles basic partial-completion)))))

;;; `marginalia'
(use-package marginalia
  :demand t
  :custom (marginalia-margin-threshold 110)
  :config
  (marginalia-mode)
  (add-to-list 'marginalia-annotator-registry '(file builtin none)))

;;; `consult'
(use-package consult
  :bind (("C-M-e" . consult-buffer)
         ("C-x b" . consult-buffer)
         ("M-s r" . consult-ripgrep)
         ("C-M-s" . consult-line)
         ("M-y"   . consult-yank-pop)
         ;; registers
         ("C-M-'" . consult-register-load)
         ("M-'"   . consult-register-store)
         ("M-\\"  . consult-register)
         ;; going to things
         ("M-g i"   . consult-imenu)
         ("M-g M-g" . consult-goto-line)
         ("M-g o"   . consult-outline)
         ("M-g m"   . consult-mark)
         ("M-g k"   . consult-global-mark))
  :config
  (slot/adjust-colours 'consult-theme)
  (use-package wgrep :demand)
  ;; No preview for these commands.
  (consult-customize consult-buffer consult-theme
                     :preview-key "M-.")
  :custom
  (consult-ripgrep-args                 ; initial arguments to `rg'
   (unwords
    "rg --null --line-buffered --color=never --smart-case --no-heading"
    "--line-number --with-filename --hidden" "--with-filename"
    "--max-columns=1000 --path-separator /"))
  (completion-in-region-function 'consult-completion-in-region)) ; complete M-:

;;; `embark'
(use-package embark
  :bind (("C-'"   . embark-act)
         ("C-c e" . embark-export)
         :map embark-region-map
         ("x"     . ix.io-paste-region)
         :map embark-sort-map
         ("h"     . haskell-sort-imports))
  :custom
  ;; Replace the key help with a completing-read interface.
  (prefix-help-command #'embark-prefix-help-command)
  :config
  ;; Hide the mode line of the Embark live/completions buffers.
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :demand
  :after consult)

;;; `corfu'
;; It's like company, but more indie!
(use-package corfu                      ; 05jun2022
  :after orderless
  :demand t
  :bind (:map corfu-map
              ("M-SPC" . nil)           ; `hippie-expand' needs this
              ("M-/"   . corfu-insert-separator))
  :custom
  (corfu-separator (string-to-char orderless-component-separator))
  (corfu-cycle t)
  (corfu-auto t)
  :config
  (global-corfu-mode)
  (corfu-history-mode)
  (corfu-indexed-mode)
  (setq completion-cycle-threshold 3) ; TAB cycle if only a few candidates
  (setq tab-always-indent 'complete)  ; Enable indentation+completion using TAB
  ;; Fix eshell
  (defun corfu-send-shell (&rest _)
    "Send completion candidate when inside eshell."
    (when (and (derived-mode-p 'eshell-mode) (fboundp 'eshell-send-input))
      (eshell-send-input)))
  (advice-add #'corfu-insert :after #'corfu-send-shell))

(provide 'completion)
