;;; latex-math.el --- LaTeX math: used by both `latex-mode' and `org-mode' ' -*- lexical-binding: t; -*-

;;; `math-delimiters'
(use-package math-delimiters ; 05sep2021
  :vc (:fetcher "github" :repo "oantolin/math-delimiters")
  :commands math-delimiters-insert
  :custom (math-delimiters-compressed-display-math nil))

;;; `latex-change-env'
(use-package latex-change-env           ; 21sep2021
  :after latex
  :preface
  (defun slot/insert-toggle-math ()
    (interactive)
    (latex-change-env-cycle '(display-math equation* align* equation align)))
  :commands latex-change-env
  :bind (:map LaTeX-mode-map
              ("C-c r" . latex-change-env))
  :custom
  (latex-change-env-math-display '("\\[" . "\\]"))
  (latex-change-env-math-inline  '("\\(" . "\\)"))
  (latex-change-env-edit-labels-in-project t))

;;; =CDLaTeX=
;; 08dec2019, 21dec2019, 08oct2020, 13nov2020, 28jan2021

;; This is like the best thing ever.  It defines all kinds of
;; abbreviations, key shortcuts etc.
(use-package cdlatex
  :bind (:map cdlatex-mode-map ("$" . nil)) ; use `latex-change-env'
  :hook
  (LaTeX-mode . turn-on-cdlatex    ) ; with `LaTeX-mode'
  (org-mode   . turn-on-org-cdlatex) ; use a subset for `org-mode'
  :custom
  (cdlatex-paired-parens "$([{")        ; pair (almost) all the things
  (cdlatex-math-modify-alist            ; math modify; accessed with '
   '(;;  MATHCMD      TEXTCMD    ARG RMDOT IT
     (?k "\\mathfrak" nil        t   nil   nil)
     (?B "\\mathbb"   "\\textbf" t   nil   nil)
     (?b "\\mathbf"   "\\textbf" t   nil   nil)
     (?f "\\mathsf"   "\\textsf" t   nil   nil)
     (?l "\\ld"       "\\textsl" t   nil   nil)
     (?u "\\lld"      ""         t   nil   nil)
     ))
  ;; Custom bindings for the symbol list, accessed with `.  You may add
  ;; additional layers here.
  (cdlatex-math-symbol-alist
   '(;;    LAYER 1        LAYER 2      LAYER 3
     (?c  ("\\circ"       ""           "\\cos"   ))
     (?C  ("\\coprod"     ""           "\\arccos"))
     (?e  ("\\varepsilon" "\\epsilon"  "\\exp"   ))
     (?f  ("\\varphi"     "\\phi"      ""        ))
     (?F  ("\\Phi"        ""           ""        ))
     (?R  ("\\real"       "\\Re"       ""        ))
     (?N  ("\\nat"        "\\nabla"    "\\exp"   ))
     (?Z  ("\\integer"    ""           ""        ))
     (?Q  ("\\rat"        "\\Theta"    ""        ))
     (?0  ("\\varnothing" ""           ""        ))
     (?{  ("\\subseteq"   "\\subset"   ""        ))
     (?}  ("\\supseteq"   "\\supset"   ""        ))
     (?.  ("\\cdot"       "\\dots"     ""        ))
     (?^  ("\\otimes"     ""           ""        ))
     (?ö  ("\\odot"       ""           ""        ))
     (?I  ("\\iota"       "\\Im"       ""        ))
     (?$  ("\\dashv"      "\\vdash"    ""        ))
     )))

;;; `aas': sane auto-expanding snippets
(use-package aas                        ; 20nov2021
  :hook ((LaTeX-mode org-mode) . aas-activate-for-major-mode)
  :config
  (--each '(latex-mode org-mode)
    (aas-set-snippets it
      :cond #'texmathp
      ":"     "\\from "
      "**"    "\\star"
      "mm"    "\\mid "
      "->"    "\\to "
      "mt"    "\\mapsto "
      "=>"    "\\implies "
      "..."   "\\dots"
      "bln"   "\\blank"
      "defn"  "\\defeq"
      "~"     "\\sim"
      "=="    "\\cong"
      "cc"    "\\cat{C}"
      "cd"    "\\cat{D}"
      "cm"    "\\cat{M}"
      "pp"    "\\mathcal{P}"
      ">"     (lambda () (interactive)
                (yas-expand-snippet "\\langle $0 \\rangle"))
      "adj"   (lambda () (interactive)
                (yas-expand-snippet "\\adj{${1:F}}{${2:U}}{${3:\\cc}}{${4:\\mm}} $0"))
      "hom"  (lambda () (interactive)
               (yas-expand-snippet "${1:\\cc}(${2:s}, ${3:t})"))
      "coend" (lambda () (interactive)
                (yas-expand-snippet "\\int^{${2:c \\in \\cat{C}}}"))
      "end"   (lambda () (interactive)
                (yas-expand-snippet "\\int_{${2:c \\in \\cat{C}}}"))
      "lr"    (lambda () (interactive)
                (yas-expand-snippet "\\left($0\\right)"))
      "set"   (lambda () (interactive)
                (yas-expand-snippet "\\\\{$0\\\\}"))
      "sum"   (lambda () (interactive)
                (yas-expand-snippet "\\sum_{${1:i=1}}^{${2:n}}$0"))
      "//"    (lambda () (interactive)
                (yas-expand-snippet "\\frac{$1}{$2}$0"))
      "cat"   (lambda () (interactive)
                (yas-expand-snippet "\\cat{$1}$0"))
      "lmod" (lambda () (interactive)
                (yas-expand-snippet "\\lmod{$1}{$2} $0"))
      "rmod" (lambda () (interactive)
                (yas-expand-snippet "\\rmod{$1}{$2} $0"))
      "bmod" (lambda () (interactive)
                (yas-expand-snippet "\\bimod{$1}{$2}{$3} $0"))
      )))

(provide 'latex-math)
;;; latex-math.el ends here
