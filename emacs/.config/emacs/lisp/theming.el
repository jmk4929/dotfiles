;;; theming.el --- Themes and fonts -*- lexical-binding: t; -*-

;;; Mode Line
;; 05aug2020, 20sep2020 `battery-mode', 30may2021 rm `battery-mode'

(use-package emacs                      ; 01jan2023 fresh start
  :preface
  (defvar slot/buffer-identification) ; 06jan2023 this is all I need pretty much
  (make-variable-buffer-local 'slot/buffer-identification)
  (put 'slot/buffer-identification 'risky-local-variable t)
  (defun slot/generate-buffer-identification ()
    (cl-flet* ((stitch-together (xs)
                 (mapconcat #'identity xs "/"))
               (shorten-directory (path max-length)
                 (-let* (((prefix . rest) (s-split "/" path 'omit-nulls)))
                   (if (<= (-sum (-map #'string-width rest)) max-length)
                       (stitch-together rest)
                     (stitch-together
                      (cons "…" (--reduce-from
                                 (if (< (-sum (-map #'string-width acc)) (- max-length 5))
                                     (cons it acc)
                                   acc)
                                 '()
                                 (nreverse rest)))))))
               (special-buffer (name)
                 (unless (or (not name) (s-contains? "*message*" name))
                   name)))
      (setq slot/buffer-identification
            (if-let* ((bfn (special-buffer (buffer-file-name)))
                      (path (abbreviate-file-name (file-name-directory bfn)))
                      (prefix (substring path 0 (+ 2 (string-match-p "[:/]" (s-chop-left 1 path)))))
                      (buffer (buffer-name))
                      (max-size (max 0 (- 40 (string-width prefix) (string-width buffer)))))
                (concat (propertize prefix 'face '(:inherit font-lock-keyword-face))
                        (shorten-directory path max-size) "/"
                        (propertize buffer 'face '(:inherit font-lock-builtin-face)))
              (propertize (buffer-name) 'face '(:inherit font-lock-builtin-face))))))
  :custom
  (mode-line-mule-info nil)
  (mode-line-position-column-line-format '("%l,%c"))
  (mode-line-buffer-identification
   '("" (slot/buffer-identification
         slot/buffer-identification
         (:eval (slot/generate-buffer-identification)))))
  :init
  (when-let ((mma (cl-member-if
                   (lambda (x) (and (listp x)
                                    (equal (car x) :propertize)
                                    (equal (cadr x) '("" minor-mode-alist))))
                   mode-line-modes)))
    ;; No minor-mode list and no parens around the major mode.
    (setcar mma nil)
    (--each '("(" ")") (delete it mode-line-modes))))

;;; Default Font
;; 09sep2020, 26dec2020, 18jan2022 stop ruining my fonts!, 15feb2023 use fontaine instead

(use-package fontaine                   ; 12feb2023
  :demand t
  :config (fontaine-set-preset 'default)
  :custom
  (fontaine-presets
   `((default :default-height  90)
     (_100    :default-height 100)
     (_120    :default-height 120)
     (_150    :default-height 150)
     (reading
      :default-family "IBM plex serif"
      :default-height 120
      :fixed-pitch-family "IBM plex serif"
      :variable-pitch-family "IBM plex serif"
      :italic-family "IBM plex serif"
      :line-spacing nil)
     (t
      :default-family "iosevka custom"
      :fixed-pitch-family "iosevka custom"
      :variable-pitch-family "IBM Plex Serif"
      :line-spacing nil))))

;; mode-line stuff; 11mar2022, 17jun2022, 14feb2023 minimise
(defun slot/set-modeline (&optional _)
  "Set the modeline height/appearance."
  (set-face-attribute 'mode-line          nil :height 80)
  (set-face-attribute 'mode-line-inactive nil :height 80))

(defun slot/LaTeX-colours (&optional _) ; 06mar2023
  "Mute LaTeX colours a little for the `modus-themes'."
  (when (eq major-mode 'latex-mode)
    (let ((remap-face (if (--any? (-contains? custom-enabled-themes it)
                                  '(modus-operandi modus-operandi-tinted modus-vivendi modus-vivendi-tinted))
                          (lambda (face  spec) (face-remap-set-base   face spec))
                        (lambda   (face _spec) (face-remap-reset-base face)))))
      (--each '(font-latex-math-face font-lock-constant-face font-lock-type-face font-lock-variable-name-face TeX-fold-folded-face)
        (funcall remap-face it `(:foreground ,(modus-themes-with-colors fg-main))))
      (--each '(font-latex-script-char-face font-lock-keyword-face font-lock-function-name-face)
        (funcall remap-face it '(:inherit (bold font-latex-math-face)))))))

(defun slot/adjust-colours (symbol)
  "Adjust colours after an event."
  (advice-add symbol :after #'slot/set-modeline)
  (advice-add symbol :after #'slot/LaTeX-colours))

(use-package emacs
  :hook ((org-mode notmuch-show-mode) . variable-pitch-mode))

;;; Themes

(use-package modus-themes ; 22mar2020 modus operandi, 29dec2022 churn…
  :demand t
  :preface
  (font-lock-add-keywords      ; 13mar2023 Make LaTeX math less visible.
   'latex-mode
   `(("\\\\[]()[]\\|\\$" 0
      '((t :inherit font-lock-comment-face :weight extra-light :slant normal)))))
  :hook (LaTeX-mode . slot/LaTeX-colours)
  :config
  ;; Refresh font settings after changing themes.
  (dolist (theme-fun '(modus-themes-load-theme modus-themes-toggle modus-themes-select))
    (slot/adjust-colours theme-fun))
  (setq modus-themes-common-palette-overrides
        `((bg-paren-match bg-magenta-intense)     ; paren-match: intense
          (bg-region bg-blue-intense)             ; region: accented
          (bg-hl-line bg-blue-nuanced)            ; hl-line: accented
          (border-mode-line-inactive unspecified) ; no borders!
          (border-mode-line-active unspecified)   ; no borders!
          ,@modus-themes-preset-overrides-faint)) ; let everything be faint!
  (modus-themes-select 'modus-operandi)
  :custom
  (modus-themes-bold-constructs t)
  (modus-themes-italic-constructs t)
  (modus-themes-variable-pitch-ui t)    ; variable pitch mode line etc.
  (modus-themes-mixed-fonts t)
  (modus-themes-headings
   '((1 . (1.3))
     (2 . (1.2))
     (3 . (1.1))
     (agenda-structure . (1.2)))))

(use-package stimmung-themes        ; 25mar2022, 04may2022
  :init (slot/adjust-colours 'stimmung-themes-toggle)
  :custom
  ;; (stimmung-themes-light-highlight-color "#f9e2ff")
  (stimmung-themes-comment 'foreground)
  (stimmung-themes-string  'foreground))

;;; `rainbow-mode'
;; Highlight hex colours with the colour they represent!
(use-package rainbow-mode               ; 16feb2021
  :hook ((text-mode prog-mode) . rainbow-mode))

(provide 'theming)
