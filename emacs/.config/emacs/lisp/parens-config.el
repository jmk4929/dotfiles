;;; parens-config.el --- Parens configuration  -*- lexical-binding: t; -*-

;;; Highlighting

(use-package paren
  :demand t
  :custom (show-paren-delay 0)
  :config (show-paren-mode))

(use-package rainbow-delimiters
  :hook ((prog-mode text-mode LaTeX-mode) . rainbow-delimiters-mode))

;;; Editing
;; 25aug2021, 31aug2021, 10sep2021, 01feb2022 smartparens -> puni

(use-package elec-pair                  ; 17jan2023
  :demand t
  :config (electric-pair-mode)
  :hook
  (org-mode . (lambda ()
                (slot/add-electric-pair '((?* . ?*) (?_ . ?_) (?/ . ?/) (?~ . ?~)))))
  (markdown-mode . (lambda ()
                     (slot/add-electric-pair '((?* . ?*) (?_ . ?_) (?` . ?`)))
                     (add-hook 'post-self-insert-hook #'slot/markdown-bold 'append t)))
  :preface
  (defun slot/add-electric-pair (pairs)
    "Add PAIRS to `electric-pair-pairs'."
    (setq-local electric-pair-pairs (append electric-pair-pairs pairs))
    (setq-local electric-pair-text-pairs electric-pair-pairs))
  (defun slot/markdown-bold ()
    "Properly insert **bold** text in `markdown-mode'."
    (when (and electric-pair-mode
               (eq last-command-event ?*)
               (eq (char-before (1- (point))) ?*)
               (not (eq (char-before (- (point) 2)) ?*)))
      (save-excursion (insert (make-string 2 ?*))))))

(use-package puni
  :hook ((text-mode prog-mode LaTeX-mode org-mode eval-expression-minibuffer-setup haskell-interactive-mode) . puni-mode)
  :bind (:map puni-mode-map
              ("C-M-e"     . nil)
              ("C-w"       . slot/backward-kill-word-dwim)
              ("C-<right>" . puni-slurp-forward)
              ("C-<left>"  . puni-barf-forward)
              ("C-)"       . puni-barf-backward)
              ("C-("       . puni-slurp-backward)
              ("C-x C-d"   . puni-splice)
              ("M-i"       . puni-squeeze)
              ("C-x C-z"   . slot/puni-rewrap))
  :preface
  (defun slot/puni-rewrap ()
    (interactive)
    (-let* ((c (read-char "Replace with? "))
            ((fst . lst) (pcase c
                           (?\( '("(" . ")"))
                           (?\) '("(" . ")"))
                           (?\{ '("{" . "}"))
                           (?\} '("{" . "}"))
                           (?\[ '("[" . "]"))
                           (?\] '("[" . "]"))
                           (?\< '("<" . ">"))
                           (?\> '("<" . ">"))
                           (?\" '("\"" . "\"")))))
      (puni-squeeze)
      (insert fst)
      (yank)
      (insert lst))))


(provide 'parens-config)
