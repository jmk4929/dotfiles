;;; dired-config.el --- Additional configuration for `dired' -*- lexical-binding: t; -*-

;;; `Dired'
(use-package dired   ; 08oct2020 better finding, 07nov2020
  :ensure nil
  :hook (dired-mode . diredfl-mode)
  :bind (:map dired-mode-map
              ("/" . dired-narrow-fuzzy) ; Fuzzy narrowing; clear with `revert-buffer'.
              ;; Reuse the same buffer.
              ("f" . dired-find-alternate-file)
              ("a" . (lambda () (interactive) (find-alternate-file ".."))))
  :custom
  (dired-dwim-target t)            ; Try to guess target directory
  (dired-listing-switches "-alh")  ; Human-readable sizes
  (dired-recursive-copies 'always) ; On `C', recursively copy by default
  (dired-omit-files       ; Don't show hidden files in `dired-omit-mode'
   "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")
  (dired-garbage-files-regexp    ; Mark these files for deletion on `%&'
   (rx "." (or "aux" "auxlock" "bbl" "blg" "out" "log" "toc" "fdb_latexmk" "synctex.gz" "fls")
       string-end))
  :config
  (use-package dired-narrow)
  (use-package diredfl)                 ; More colorful output
  (use-package dired-x                  ; Cool Extra functionality
    :demand
    :ensure nil))

;;; Deleting Files and Buffers
;; 28aug2020

;; Rename/delete files and the delete associated buffers if needed.
(defun slot/rename-kill-current-file ()
  "Rename the current file, ask to delete the old buffer, and open
the new file."
  (interactive)
  (let ((new-file-name (read-file-name "Enter new file name: ")))
    (progn
      (save-buffer)
      (rename-file buffer-file-name new-file-name)
      (when (y-or-n-p "Delete the old buffer?")
        (kill-current-buffer))
      (find-file new-file-name))))

(defun slot/kill-delete-current-file ()
  "Delete the current file and ask to do the same for the
still-open buffer.  This is the nuclear option."
  (interactive)
  (delete-file buffer-file-name)
  (when (y-or-n-p "Delete the old buffer?")
    (kill-current-buffer)))

(provide 'dired-config)
