;;; org-setup.el --- My `org-mode' configuration -*- lexical-binding: t; -*-

;;; Basic Org Config

(use-package org
  :vc                                   ; 25mar2023 new LaTeX preview
  (org-mode . (:url "https://git.tecosaur.net/tec/org-mode.git" :branch "dev"))
  :preface
  (defun slot/in-org-dir (file)
    (concat (file-name-as-directory org-directory) file))
  :hook
  ;; Don't highlight my Haskell arrows!
  (org-mode . (lambda ()
                (modify-syntax-entry ?< "." org-mode-syntax-table)
                (modify-syntax-entry ?> "." org-mode-syntax-table)))
  (org-mode . org-indent-mode)
  :bind ((:map org-mode-map
               ("C-c l"       . org-auctex-preview-buffer)
               ("C-c C-x C-s" . org-archive-subtree   )
               ("C-c c"       . org-capture           )
               ("$"           . math-delimiters-insert))
         (:repeat-map org-repeat-map    ; 07jul2022
                      ("p"   . org-previous-visible-heading)
                      ("n"   . org-next-visible-heading)
                      ("TAB" . org-cycle)))
  :custom
  (org-directory "~/repos/org")
  (org-archive-location (concat (slot/in-org-dir "archive.org") "::* From %s"))
  (org-ellipsis "…")
  (org-hide-leading-stars t)        ; Hide leading stars in subheadings.
  (org-log-done 'time)              ; Record time when archiving.
  (org-log-repeat nil)              ; Record nothing then repeating
  (org-refile-targets '((org-agenda-files :todo . "PROJECT")))
  (org-todo-keywords
   '((sequence "TODO(t)" "NEXT(n)" "WAITING(w)" "CURRENT(C)" "|" "DONE(d)"    )
     (sequence "BUG(b)" "HOLD(h)" "SOMEDAY(s)"               "|"              )
     (sequence "PROJECT(p)"                                  "|" "CANCELED(c)")))
  (org-capture-templates
   `(("t" "Todo"                entry (file ,(slot/in-org-dir "todos.org")) "* TODO %?\n %i\n %a")
     ("T" "Todo with Clipboard" entry (file ,(slot/in-org-dir "todos.org")) "* TODO %?\n %i\n %a")
     ("m" "Mail Todo"           entry (file ,(slot/in-org-dir "todos.org")) "* TODO %A %?")))
  (org-modules '(org-habit))
  :config
  (require 'org-protocol)
  (add-to-list 'org-export-backends 'md) ; Export to more formats
  ;; If `org-open-at-point' wants to open a file, open it in this frame.
  (add-to-list 'org-link-frame-setup '(file . find-file)))

;; Like `prettify-symbols-unprettify-at-point', but for *boldface*,
;; /italics/, etc. in `org-mode'.
(use-package org-appear                 ; 22jan2022
  :hook (org-mode . org-appear-mode)
  :custom (org-hide-emphasis-markers t))

(use-package org-latex-preview          ; 28mar2023
  :after org
  :ensure nil
  :hook (org-mode . org-latex-preview-auto-mode)
  :custom
  (org-latex-preview-default-process 'dvisvgm)
  (org-latex-preview-options '(:foreground auto :background "Transparent" :scale 1.2
                               :matchers ("begin" "$1" "$" "$$" "\\(" "\\[") :zoom 1.2))
  (org-latex-preview-numbered t))

;;; Agenda

;;;; `khalel'
(use-package khalel                     ; 16apr2022
  :commands khalel-run-vdirsyncer
  :preface
  (defun slot/nextcloud-update ()
    (interactive)
    (khalel-run-vdirsyncer)
    (khalel-import-events))
  :init (add-to-list 'safe-local-variable-values '(buffer-read-only . 1))
  :config (khalel-add-capture-template)
  :custom
  (khalel-default-calendar nil)         ; Configured via `khal'
  (khalel-import-org-file-confirm-overwrite nil)
  (khalel-import-org-file (concat org-directory "/calendar.org")))

;;;; `org-agenda'
(use-package org-agenda
  :ensure nil
  :hook (org-agenda-mode . olivetti-mode)
  :preface
  (defun slot/org-agenda-skip-tag (tag)
    "Context: point is currently on a headline."
    (when (member tag (org-get-tags (point)))
      (outline-next-heading)))
  :bind (:map org-agenda-mode-map
              ("f" . org-agenda-date-later)
              ("M-f" . org-agenda-later))
  :custom
  (org-agenda-tags-column 77)           ; Show tags in colum 77.
  (org-agenda-prefix-format '((agenda . "  %?-12t% s"))) ; " In x d.:  "
  (org-agenda-custom-commands
   `(("n" "Block Agenda"
      ((agenda "Current day"
               ((org-agenda-block-separator nil)
                (org-agenda-span 1)
                (org-deadline-warning-days 0)
                (org-agenda-overriding-header (format-time-string "%a, %d %b %Y"))
                (org-agenda-format-date "")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'regexp ".*NEHMEN:.*"))))
       (agenda "Take this"
               ((org-agenda-block-separator nil)
                (org-agenda-span 1)
                (org-deadline-warning-days 0)
                (org-agenda-overriding-header "")
                (org-agenda-format-date "")
                (org-agenda-skip-function
                 '(org-agenda-skip-entry-if 'notregexp ".*NEHMEN:.*"))))
       (agenda "Upcoming deadlines and schedules"
               ((org-agenda-block-separator (concat (-repeat 105 9472)))
                (org-agenda-entry-types '(:deadline :scheduled))
                (org-agenda-span 13)
                (org-agenda-start-day "+1d")
                (org-agenda-time-grid nil)
                (org-agenda-skip-function
                 '(or (org-agenda-skip-entry-if 'regexp ".*NEHMEN:.*")
                      (slot/org-agenda-skip-tag "habit")))
                (org-agenda-overriding-header "\nDeadlines and Schedules\n")
                (org-deadline-warning-days 0))))))))

;;; `org-roam'
(use-package org-roam ; 12oct2020, 22nov2020, 21jul2021 v2
  :after org
  :preface

  (defun slot/org-roam-insert-image ()
    "Select and insert an image at point."
    (interactive)
    (let* ((file-name (format "%s-%s.png"
                              (file-name-sans-extension (buffer-name))
                              (cl-random (expt 2 31))))
           (path (format "%s/%s/%s" org-roam-directory "images" file-name)))
      ;; The mouse movement via xdotool is needed because otherwise, if
      ;; unclutter is active, the pointer will remain hidden.  Uff.
      (call-process "xdotool" nil 0 nil "mousemove_relative" "--" "-1" "0")
      (let ((scrot-exit (call-process "scrot" nil nil nil "-z" "-f" "-s" "--file" path)))
        (when (= scrot-exit 0)
          (insert (format "[[../images/%s]]" file-name))))))

  (defun slot/org-roam-process-before-exporting (_)
    "Properly process an `org-roam' file before exporting.
In particular, kill all the cross-references and just replace
them with their respective descriptions."
    (when (and (boundp 'org-roam-mode) org-roam-mode)
      (replace-regexp (rx "[[" (one-or-more (not ?\]))         ; id
                          "][" (group (one-or-more (not ?\]))) ; description
                          "]]")
                      "\\1")))

  (defun slot/org-roam-capture-new-node ()
    (org-roam-tag-add '("draft"))
    (when (s-prefix? (concat org-roam-directory "/novel") (buffer-file-name))
      (org-roam-tag-add '("novel"))))

  :hook
  (org-roam-capture-new-node . slot/org-roam-capture-new-node)
  (org-export-before-processing . slot/org-roam-process-before-exporting)
  :bind (:map org-mode-map
              ("C-c n i" . org-roam-node-insert)
              ("C-c n l" . org-roam-buffer-toggle)
              ("C-c n f" . org-roam-node-find)
              ("C-c n g" . org-roam-graph)
              ("C-c n c" . org-roam-capture)
              ("C-c M-i" . slot/org-roam-insert-image))
  :custom
  (org-roam-directory (file-truename "~/repos/org/org-roam"))
  (org-roam-node-display-template
   (concat "${type:10} ${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-capture-templates
   '(("n" "novel" plain
      "%?"
      :if-new (file+head "novel/${slug}.org" "#+title: ${title}\n#+startup: inlineimages\n#+latex_header: \\usepackage[org]{$HOME/.tex/styles/style}\n\n")
      :immediate-finish t
      :unnarrowed t)
     ("r" "reference" plain
      "%?"
      :if-new (file+head "reference/${slug}.org" "#+title: ${title}\n#+startup: inlineimages\n#+latex_header: \\usepackage[org]{$HOME/.tex/styles/style}\n\n")
      :immediate-finish t
      :unnarrowed t)))
  :config
  (org-roam-setup)
  (require 'org-roam-protocol)
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node) org-roam-directory))))
      (error ""))))

(use-package org-roam-ui
  :after org-roam
  :custom (org-roam-ui-latex-macros
           '(("\\defeq" . ":=")
             ("\\blank" . "{-}")
             ("\\nt"    . "\\implies")
             ("\\cc"    . "\\mathcal{C}")
             ("\\cat"   . "\\mathcal{#1}")
             ("\\from"  . "\\colon")
             ("\\id"    . "\\mathrm{id}")
             ("\\Id"    . "\\mathrm{Id}"))))

;;; `org-present'
;; Presentation in `org-mode' are really cool.  This horrible collection
;; of the most despicable hacks configures `org-present' exactly to my
;; liking.  There are probably much better ways to do this but, alas, I
;; don't know them and so this is what we're stuck with for now.
(use-package org-present                ; 07oct2020
  :disabled t                           ; 29mar2023
  :preface
  (defun slot/org-present-start ()
    ;; (setq header-line-format " ")
    (setq scroll-margin 0)
    (org-present-big)
    (org-display-inline-images)
    (mini-modeline-mode -1)
    (hide-mode-line-mode 1)
    (company-mode -1)
    (flyspell-mode -1)
    (rainbow-delimiters-mode -1)
    (setq olivetti-body-width 88)
    (setq org-hide-emphasis-markers t))

  (defun slot/org-present-stop ()
    ;; (setq header-line-format nil)
    (setq scroll-margin 2)
    (org-present-small)
    (org-remove-inline-images)
    (hide-mode-line-mode -1)
    (mini-modeline-mode 1)
    (company-mode 1)
    (flyspell-mode 1)
    (rainbow-delimiters-mode 1)
    (olivetti-mode -1)
    (setq org-hide-emphasis-markers nil))

  :hook
  (org-present-mode      . slot/org-present-start)
  (org-present-mode-quit . slot/org-present-stop)
  :bind
  (:map org-present-mode-keymap ;; HACK HACK HACK
        ("<right>" . nil)
        ("<left>"  . nil)
        ("C-c f"   . (lambda () (interactive) (org-present-next) (org-cycle) (org-cycle)))
        ("C-c p"   . (lambda () (interactive) (org-present-prev) (org-cycle) (org-cycle)))
        ("C-c C-y" . (lambda () (interactive) (org-next-visible-heading 1) (org-cycle 3) (recenter-top-bottom 1)))
        ("C-c C-e" . (lambda () (interactive) (org-next-visible-heading 1) (org-cycle 3)))))

;;; `anki-editor'
(use-package anki-editor                ; 15feb2023
  :vc (:fetcher github :repo "orgtre/anki-editor")
  :commands anki-editor-mode
  :custom (anki-editor-create-decks t)
  :config (use-package anki-editor-ui
            :ensure nil
            :bind (:map anki-editor-mode-map
	                ("C-c C-a" . anki-editor-ui))))

(provide 'org-setup)
