;;; mailboxes.el --- My mailboxes -*- lexical-binding: t; -*-

(advice-add #'notmuch-mua-send-and-exit :after
            (lambda ()
              (interactive)
              (shell-command "notmuch tag -unread from:tony.zorman@tu-dresden.de from:tonyzorman@mailbox.org from:soliditsallgood@mailbox.org tag:unread")))

;; SMTP default settings.
(use-package smtpmail
  :custom
  (user-mail-address            "tony.zorman@tu-dresden.de")
  (smtpmail-default-smtp-server "msx.tu-dresden.de")
  (smtpmail-smtp-server         "msx.tu-dresden.de")
  (send-mail-function           #'smtpmail-send-it)
  (smtpmail-stream-type         'starttls)
  (message-send-mail-function   #'smtpmail-send-it)
  (smtpmail-smtp-service        587))

(defconst slot/email-signature "Tony Zorman | https://tony-zorman.com/")

;; Define identities for different addresses.
(use-package gnus-alias
  :hook (message-setup . gnus-alias-determine-identity)
  :custom
  ;; Define identities
  (gnus-alias-identity-alist
   '(("uni"
      nil ;; Refer to other identities
      "Tony Zorman <tony.zorman@tu-dresden.de>" ; Sender address
      nil                                       ; Organization header
      nil                                       ; Extra headers
      nil                                       ; Extra body text
      slot/email-signature)                     ; Signature
     ("mailbox"
      nil
      "Tony Zorman <tonyzorman@mailbox.org>"
      nil
      (("X-Message-SMTP-Method" . "smtp smtp.mailbox.org 587 tonyzorman@mailbox.org"))
      nil
      slot/email-signature)
     ("mailbox-slot"
      nil
      "Tony Zorman <soliditsallgood@mailbox.org>"
      nil
      (("X-Message-SMTP-Method" . "smtp smtp.mailbox.org 587 soliditsallgood@mailbox.org"))
      nil
      slot/email-signature)))

  ;; Match with default SMTP settings above
  (gnus-alias-default-identity "uni")

  ;; Define rules to match other identities
  (gnus-alias-identity-rules
   '(("mailbox"      ("any" "tonyzorman@mailbox\\.org"      both) "mailbox"     )
     ("mailbox-slot" ("any" "soliditsallgood@mailbox\\.org" both) "mailbox-slot"))))

;; Send mail should go into the appropriate directories.
(setq notmuch-fcc-dirs
      '(("tony.zorman@tu-dresden.de"   . "uni/Sent"         )
        ("tonyzorman@mailbox.org"      . "mailbox/Sent"     )
        ("soliditsallgood@mailbox.org" . "mailbox-slot/Sent")))

(provide 'mailboxes)
;;; mailboxes.el ends here
