 ;;; ghci-conf-mode.el --- Font locking for ghci config files -*- lexical-binding: t -*-

;; Copyright 2020  slotThe
;; URL: https://github.com/slotThe/dotfiles
;; Version: 0.0.1

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Major mode for ghci configuration files.

;;; Code:

(defgroup ghci-conf nil
  "Major mode for ghci configuration files."
  :group 'languages)

;;; Vars

(defvar ghci-conf-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; Comments.
    (modify-syntax-entry ?\- ". 12b" table)
    (modify-syntax-entry ?\n "> b"   table)
    table)
  "Syntax table for `ghci-conf-mode'.")

;;; Define Major Mode

;;;###autoload
(define-derived-mode ghci-conf-mode conf-space-mode "Ghci-Conf"
  "Simple major mode for ghci configuration files."
  (set-syntax-table ghci-conf-mode-syntax-table))

;;;###autoload
(add-to-list 'auto-mode-alist '("ghci.conf" . ghci-conf-mode))

(provide 'ghci-conf-mode)

;;; ghci-conf-mode.el ends here
