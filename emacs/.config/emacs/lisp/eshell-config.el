;;; eshell-config.el --- My eshell config -*- lexical-binding: t; -*-
;; 20mar2022, 05jun2022

(use-package eshell
  :preface
  (defun slot/eshell-exit (&optional arg)
    "Exit eshell and kill the current frame."
    (interactive "P")
    (kill-current-buffer)
    (save-buffers-kill-terminal arg))

  (defun slot/eshell-insert-history ()
    "Interactively insert a history item into eshell."
    (interactive)
    (insert
     (completing-read "Eshell history: "
                      (delete-dups (ring-elements eshell-history-ring)))))
  :hook
  (eshell-mode . (lambda ()
                   (setq-local read-process-output-max (* 1024 1024))
                   (setq-local corfu-auto nil)
                   (corfu-mode)))
  :bind (:map eshell-mode-map
              ("C-x C-c" . slot/eshell-exit)
              :map eshell-hist-mode-map
              ("M-r" . slot/eshell-insert-history))
  :config
  (require 'tramp)
  (require 'em-tramp)                   ; for `sudo'; see aliases
  (require 'em-hist)
  (require 'em-term)
  (add-to-list 'eshell-visual-commands "nmtui")
  :custom
  (password-cache-expiry (* 5 60))
  (eshell-history-file-name "~/.config/zsh/zsh_history")
  (eshell-history-size 100000)
  (eshell-hist-ignoredups t)
  (eshell-error-if-no-glob t)
  (eshell-destroy-buffer-when-process-dies t)
  (eshell-prompt-regexp "└─[$#] ")      ; bottom of prompt
  (eshell-prompt-function #'slot/default-prompt-function))

(use-package em-smart
  :ensure nil
  :hook (eshell-mode . eshell-smart-initialize))

(use-package esh-autosuggest            ; fish-like autosuggestions
  ;; 15feb2023 github:dieggsy/esh-autosuggest#15 not merged yet.
  :vc (:fetcher github :repo "slotThe/esh-autosuggest")
  :hook (eshell-mode . esh-autosuggest-mode))

(use-package eshell-toggle
  :bind ("M-`" . eshell-toggle)
  :custom
  (eshell-toggle-size-fraction 3)
  (eshell-toggle-init-function (lambda (_dir) (project-eshell))))

;; https://github.com/exot/.emacs.d/blob/5009eb39f98edeb92d6f03089615c314b2043c45/site-lisp/db-eshell.el#L56
;; https://github.com/howardabrams/dot-files/blob/master/emacs-eshell.org
(defun slot/default-prompt-function ()
  "A prompt for eshell of the form
   ┌─[$USER@$HOST] [$PWD] [$GIT]
   └─"
  (let ((head-face '(:foreground "#315b00"))
        (pwd (abbreviate-file-name (eshell/pwd))))
    (cl-flet ((git-info ()
                (when (and (not (file-remote-p pwd))
                           (eshell-search-path "git")
                           (locate-dominating-file pwd ".git"))
                  (let ((git-url (shell-command-to-string "git config --get remote.origin.url"))
                        (git-output (shell-command-to-string "git rev-parse --abbrev-ref HEAD")))
                    (concat (file-name-base (s-trim git-url)) " \xe0a0 " (s-trim git-output))))))
      (concat (propertize "┌─" 'face head-face)
              (user-login-name)
              "@"
              (system-name)
              " "
              (propertize pwd 'face '((:foreground "#721045")))
              " "
              (git-info)
              "\n"
              (propertize "└─" 'face head-face)
              (if (zerop (user-uid)) "#" "$")
              (propertize " " 'face '(:weight bold))))))

(provide 'eshell-config)
;;; eshell-config.el ends here
