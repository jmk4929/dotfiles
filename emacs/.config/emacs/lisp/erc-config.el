;;; erc-config.el --- My ERC configuration -*- lexical-binding: t; -*-

;;; `erc'

(use-package erc                        ; 20sep2022
  :commands erc-tls
  :hook
  (erc-text-matched . slot/erc-log-matches)
  (erc-text-matched . slot/beep-on-match)
  :bind (:map erc-mode-map
              ("C-t"     . erc-track-switch-buffer)
              ("C-x C-c" . slot/erc-quit))
  :config
  (use-package erc-hl-nicks)
  (--each '(log hl-nicks pcomplete scrolltobottom)
    (add-to-list 'erc-modules it))
  (erc-update-modules)
  (erc-bar-setup)
  ;; Treat queries as always highlighting me.
  (defadvice erc-track-find-face (around erc-track-find-face-promote-query activate)
    (if (erc-query-buffer-p)
        (setq ad-return-value (intern "erc-current-nick-face"))
      ad-do-it))
  ;; When changing themes, make sure that nicknames are still readable.
  (advice-add #'consult-theme       :after (lambda (_) (erc-hl-nicks-refresh-colors)))
  (advice-add #'modus-themes-toggle :after (lambda ()  (erc-hl-nicks-refresh-colors)))
  :preface
  (defun slot/erc-quit ()
    (interactive)
    (erc-log-save-all-buffers)
    (erc-cmd-GQUIT "")
    (sleep-for 1)
    (slot/kill-emacs))

  (defun slot/erc-ignore-highlight (msg)
    "Don't highlight me when these things happen."
    (let ((message (s-trim-left msg))
          (channel (or (erc-default-target) "")))
      (--any? (s-prefix? it message)
              `("*** Users on"
                "*** Your new nickname is"
                "*** Welcome to the"
                ,(concat "*** " channel ": topic set by")))))

  (defun slot/erc-log-matches (match-type nickuserhost message)
    "Log matches to extra buffer, unless they are annoying."
    (unless (slot/erc-ignore-highlight message)
      (erc-log-matches match-type nickuserhost message)))

  (defun slot/mark-emacs-urgent ()
    "Mark the current frame as urgent."
    (let* ((WM-HINTS "WM_HINTS")
           (hints (seq--into-list
                   ;; By default this returns a string/vector.
                   (x-window-property WM-HINTS nil WM-HINTS nil nil t))))
      ;; Urgency flag: (1L << 8) == 256
      ;; Source (as always): https://tronche.com/gui/x/xlib/ICC/client-to-window-manager/wm-hints.html
      (setcar hints (logior (car hints) (lsh 1 8)))
      (x-change-window-property WM-HINTS hints nil WM-HINTS 32)))

  (defun slot/beep-on-match (match-type _nickuserhost message)
    "Beep and mark the frame as urgent on highlight."
    (let ((visible-bell nil))
      (unless (slot/erc-ignore-highlight message)
        (slot/mark-emacs-urgent)
        (erc-beep-on-match match-type _nickuserhost message))))

  (defun slot/erc-channel-users (&rest ignore)
    "Display how many users (and ops) the current channel has."
    (let ((users 0) (ops 0))
      (if (not (hash-table-p erc-channel-users))
          ""
        (maphash (lambda (k _v)
                   (cl-incf users)
                   (when (erc-channel-user-op-p k)
                     (cl-incf ops)))
                 erc-channel-users)
        (pcase (cons (= 0 ops) (= 0 users))
          ('(t . t  ) "")
          ('(t . nil) (format "[%s] " users))
          (_          (format "[%s (@%s)] " users ops))))))

  (defun slot/erc-generate-log-file-name (_buffer target _nick server _port)
    "Save logs in #channel@server.txt format."
    (convert-standard-filename (concat target "@" server ".txt")))

  :custom
  (erc-track-switch-direction 'importance)
;;;; Highlighting
  (erc-log-matches-flag t)                       ; always log highlights
  (erc-beep-match-types '(current-nick keyword)) ; beep on all highlights
  (erc-keyword-highlight-type 'message)
  (erc-keywords (--map (concat "\\b" it "\\b") ; match as separate word
                       '("[Ss]olid[-_]*" "[Ss]oleed" "[Ss]lut" "slot\\([Tt]he\\)*"
                         "[Tt]ony" "[Zz]orman"
                         "luxIrcBridge"
                         "[Hh]askell")))
;;;; Prompt
  (erc-prompt (lambda () (format "%s%s ⟩" (slot/erc-channel-users) (buffer-name))))
;;;; Hide stuff; see https://modern.ircdocs.horse for codes.
  (erc-track-exclude-types '("JOIN" "KICK" "NICK" "PART" "QUIT" "MODE" "333" "353"))
  (erc-hide-list '("JOIN" "PART" "QUIT"))
;;;; Timestamps
  (erc-timestamp-only-if-changed-flag nil) ; always show timestamp
  (erc-timestamp-format "[%Y-%m-%d %H:%M]")
  (erc-insert-timestamp-function #'erc-insert-timestamp-left)
;;;; Filling
  (erc-fill-column 170)
  (erc-fill-function #'erc-fill-static) ; align nicknames
  (erc-fill-static-center 20)
;;;; Logging
  (erc-log-channels-directory (concat user-emacs-directory "erc/logs/"))
  (erc-generate-log-file-name-function #'slot/erc-generate-log-file-name)
  (erc-log-write-after-insert t)
  (erc-log-write-after-send t)
  (erc-save-buffer-on-partn t)
  (erc-save-queries-on-quit t))

(use-package erc-tex
  :commands erc-tex-mode
  :after erc
  :preface
  (defun slot/erc-tex-mode ()
    "Only render LaTeX in some channels."
    (let ((channel (buffer-name)))
      (if (-contains? '("##WIL.fresh.random" "##WIL.fresh.stupid"
                        "##WIL.fresh.quarantine" "##WIL.fresh.challenges")
                      channel)
          (erc-tex-mode 1)
        (erc-tex-mode -1))))
  :hook (erc-join . slot/erc-tex-mode)
  :ensure nil)

;;; `erc-bar': a bar as an unread indicator, like weechat has.
;; 21sep2022

(defcustom erc-bar-threshold 1
  "Display bar when there are more than this many unread messages.")

(defun erc-bar-move-back (n)
  "Moves back n message lines.
Ignores wrapping and server messages."
  (interactive "nHow many lines ? ")
  (re-search-backward "^.*<.*>" nil t n))

(defun erc-bar-update-overlay ()
  "Update the overlay for current buffer.
The update itself is based on the content of
`erc-modified-channels-alist'; this function should be executed
on window change."
  (interactive)
  (let* ((info (assq (current-buffer) erc-modified-channels-alist))
	 (count (cadr info)))
    (if (and info (> count erc-bar-threshold))
	(save-excursion
	  (end-of-buffer)
	  (when (erc-bar-move-back count)
	    (let ((inhibit-field-text-motion t))
	      (move-overlay erc-bar-overlay
	                    (line-beginning-position)
	                    (line-end-position)))))
      (delete-overlay erc-bar-overlay))))

(defvar erc-bar-overlay nil
  "Overlay used to set bar")
(setq erc-bar-overlay (make-overlay 0 0))
(overlay-put erc-bar-overlay 'face '(:underline "purple"))

(defun erc-bar-setup ()
  ;; Put the hook before `erc-modified-channels-update'.
  (advice-add #'erc-track-mode :after
              (lambda (&optional _)
                ;; Remove and add, so we know it's in the first place.
                (remove-hook 'window-configuration-change-hook #'erc-bar-update-overlay)
                (add-hook    'window-configuration-change-hook #'erc-bar-update-overlay)))
  (add-hook 'erc-send-completed-hook (lambda (_str) (erc-bar-update-overlay))))

(provide 'erc-config)
;;; erc-config.el ends here
