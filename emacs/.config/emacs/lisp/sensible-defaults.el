;;; sensible-defaults.el --- Finally, sensible defaults for Emacs! -*- lexical-binding: t; -*-

(use-package emacs
  :preface
  (defun slot/kill-emacs ()
    "Kill the Emacs server.
Be sure to save things before, though."
    (interactive)
    (save-some-buffers)                 ; really making sure
    (save-buffers-kill-emacs))
  :config
  (setq-default cursor-type 'hbar)
  (setq kill-do-not-save-duplicates t) ; Don't save duplicates to kill ring
  (setq mouse-yank-at-point   t) ; Yank text to point, not to mouse cursor
  (setq words-include-escapes t) ; Treat escape chars as part of words
  (setq truncate-string-ellipsis "…")
;;;; https://trojansource.codes/trojan-source.pdf
  (add-hook 'prog-mode-hook #'glyphless-display-mode)
  (setq-default bidi-paragraph-direction 'left-to-right
                bidi-inhibit-bpa         t)
;;;; Emacs prompting me
  (setq use-short-answers     t)    ; Why would you ever want to type a full yes?
  (setq y-or-n-p-use-read-key t)    ; read-key instead of minibuffer (more flexible)
;;;;  Emacs trying to "help" me.
  (setq visible-bell t)             ; Visual bell instead of audio bell.
  (setq disabled-command-function nil)  ; Allow all the things
;;;; Whitespace, Newlines, Line Breaks
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (setq require-final-newline t)    ; Require a final newline—POSIX BOIS
  (setq-default fill-column 72)     ; Default column line length.
;;;; Scrolling Behaviour
  (setq
   next-screen-context-lines 4        ; Preserve this many lines when jumping a screenful.
   scroll-margin 2                    ; Scroll when cursor is this many lines to screen edge.
   scroll-conservatively 50           ; Only 'jump' when moving this far off the screen.
   scroll-preserve-screen-position t) ; preserve line/column (nicer C-f, C-v, M-v, C-b, C-u, etc.)
;;;; Backups
  (setq-default
   backup-directory-alist       ; Where to store backup files
   `(("." . ,(expand-file-name "backups/" user-emacs-directory)))
   backup-by-copying   t        ; Copy the whole file when backing it up
   version-control     t        ; Verison numbers for backup files
   delete-old-versions t        ; Delete excess backup version silently
   kept-old-versions   5
   kept-new-versions   5)
;;;; Opening things             ; 14oct2022
  (use-package browse-url
    :custom
    (browse-url-generic-program "decide-link.sh")
    (browse-url-browser-function #'browse-url-generic)))

;;; Line and Column Numbers

;; 12oct2020, 17jan2022 remove keybinding, 12feb2022 remove fn
(use-package emacs
  :custom (column-number-mode t)        ; for the modeline
  :config (advice-add 'display-line-numbers-mode :after
                      (lambda (_)
                        (when display-line-numbers-mode
                          (setq display-line-numbers 'visual
                                display-line-numbers-current-absolute t)))))

;;; Utility

;; 27oct2021
(defun unwords (&rest xs)
  (mapconcat #'identity xs " "))

;; 01sep2022
(defconst laptop-name  "pbox")
(defconst desktop-name "nix-box")

;; 21sep2021, 02sep2022
(defmacro defrepeatmap (symbol pairs &optional docstring)
  "A macro for defining `repeat-map's.
Defines a new repeat-map called SYMBOL with the given DOCSTRING.
The keys are derived via the list PAIRS, whose elements are cons
cells of the form (KEY . DEF), where KEY and DEF must fulfill the
same requirements as if given to `keymap-set'.

If the key only consists of a single character; i.e., is already
bound and a repeat map is created afterwards, simply add it to
the repeat-map SYMBOL.  If not, globally bind KEY to DEF and only
insert the last character of DEF into the repeat map SYMBOL."
  (declare (indent 1) (debug t))
  `(progn
     (defvar ,symbol
       (let ((map (make-sparse-keymap)))
         (--each ,pairs
           (-let (((key . fun) it))
             (if (length= key 1)
                 (keymap-set map key fun)
               (bind-key (kbd key) fun)
               (keymap-set map (s-right 1 key) fun))))
         map)
       ,docstring)
     ;; Tell the keys they are in a repeat map.
     (--each (mapcar 'cdr (cdr ,symbol))
       (put it 'repeat-map ',symbol))))

(provide 'sensible-defaults)
