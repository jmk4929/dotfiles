;;; startup-screen.el --- A minimal startup screen -*- lexical-binding: t; -*-

;;; Code:

(defun slot/startup-screen ()
  "Customize the startup screen."
  (setq initial-major-mode 'fundamental-mode
        initial-scratch-message (unwords ";;" (slot/random-startup-msg))))

(defun slot/random-startup-msg ()
  "Get a randomized startup message—we like to have fun in life."
  (let ((msgs '("Welcome to Emacs, the thermonuclear editor."
                "Nice day for Emacsing!"
                "The one true editor, Emacs!"
                "Who the hell uses VIM anyway? Go Evil!"
                "Free as free speech, free as free Beer."
                "Richard Stallman is proud of you."
                "Happy coding!"
                "Vi Vi Vi, the editor of the beast."
                "Welcome to the church of Emacs."
                "While any text editor can save your files, only Emacs can save your soul."
                "You'd be better off selling T-shirts that say \"WHAT PART OF\" (and then the Hindley–Milner prinicipal-type algorithm) \"DON'T YOU UNDERSTAND?\"."
                "A comathematician is someone who turns cotheorems into ffee."
                "Don't drink and derive!"
                "Carpe diem. Seize the day, boys. Make your lives extraordinary."
                "My precious."
                "Mama always said life was like a box of chocolates. You never know what you're gonna get."
                "I am big! It's the pictures that got small."
                "The stuff that dreams are made of."
                "I love the smell of napalm in the morning."
                "A coconut is really just a nut."
                "I showed you my source code, pls respond"
                "I’m using Linux. A library that Emacs uses to communicate with Intel hardware.")))
    (nth (random (length msgs)) msgs)))

(add-hook 'after-init-hook 'slot/startup-screen)

(provide 'startup-screen)
;;; startup-screen.el ends here
