;;; writing.el --- Configurations relating to writing stuff -*- lexical-binding: t; -*-

;;; Spell-Checking
;; Pretty standard spell-checking with `ispell' and `flyspell'.  I use
;; `hunspell' for the actual dicts, because (unlike `aspell') it can
;; have multiple dictionaries for different languages at the same time.
(use-package ispell
  :when (string= system-name laptop-name)
  :config
  (setenv "LANG" "en_GB")                 ; Needed for some reason.
  (setq ispell-program-name "hunspell")   ; Supports multiple dicts.
  ;; Dictionaries to use.
  ;; Also see https://github.com/hunspell/hunspell/issues/641
  (setq ispell-dictionary "en_CUSTOM,de_DE") ; http://app.aspell.net/create
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic ispell-dictionary)
  ;; File to store words in, create it if necessary.
  (setq ispell-personal-dictionary "~/.config/hunspell/personal-dict")
  (unless (file-exists-p ispell-personal-dictionary)
    (shell-command (unwords "touch" ispell-personal-dictionary))))

;; Tases you if you forget a letter.
(use-package flyspell
  :hook ((prog-mode . flyspell-prog-mode)
         ((git-commit-mode org-mode text-mode LaTeX-mode) . flyspell-mode))
  :bind (:map flyspell-mode-map ("C-." . nil)) ; `avy' needs that
  :custom
  ;; Do not print messages for every word (when checking the entire
  ;; buffer).  This is a major performance gain.
  (flyspell-issue-message-flag nil))

(provide 'writing)
