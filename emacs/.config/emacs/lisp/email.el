;;; email.el --- Settings for my email client -*- lexical-binding: t; -*-

;;; Email in Emacs, of course!
;; 18apr2020, 06jun2020 notmuch, 01oct2020 move smtp settings to `mailboxes.el', 12jan2021
(use-package notmuch
  :load-path "/usr/share/emacs/site-lisp/notmuch/"
  :commands notmuch
  :preface
  (defun slot/capture-mail ()
    (interactive)
    (org-capture nil "m"))

  (defun slot/encrypt-message ()
    (interactive)
    (mml-secure-message-sign-encrypt)
    (notmuch-mua-send-and-exit))

  (defun slot/sign-message ()
    (interactive)
    (mml-secure-message-sign)
    (notmuch-mua-send-and-exit))

  (defun slot/mark-as-deleted (beg end)
    "Mark message as deleted.  Note that this will not actually cause
  the deletion of the message; that job will be done by the mail
  fetching script)."
    (interactive (notmuch-search-interactive-region))
    (slot/mark-as '("+delete" "-inbox" "-unread") beg end))

  (defun slot/mark-as-read (beg end)
    "Mark message as read."
    (interactive (notmuch-search-interactive-region))
    (slot/mark-as '("-unread") beg end))

  (defun slot/mark-as (lst beg end)
    (notmuch-search-tag lst beg end)
    (notmuch-search-next-thread))

  :hook ((notmuch-message-mode . flyspell-mode)
         (notmuch-mua-send . notmuch-mua-attachment-check))
  :bind (:map message-mode-map
              ("C-a"     . slot/back-to-indentation)
              :map notmuch-show-mode-map
              ("C-c l"   . slot/capture-mail)
              ("o"       . notmuch-show-interactively-view-part)
              ("C-c C-o" . org-open-at-point)
              :map notmuch-message-mode-map
              ("C-c C-e" . slot/encrypt-message)
              ("C-c C-s" . slot/sign-message)
              ("C-c C-c" . notmuch-mua-send-and-exit)
              :map notmuch-hello-mode-map
              ("s"       . consult-notmuch)
              ("S"       . notmuch-search)
              :map notmuch-search-mode-map
              ("s"       . consult-notmuch)
              ("S"       . notmuch-search)
              ("C-c C-r" . slot/mark-as-read)
              ("D"       . slot/mark-as-deleted))
  :custom
  (notmuch-poll-script "fetch-and-sync-mail.sh")
  (message-kill-buffer-on-exit t)
  (notmuch-search-oldest-first nil)     ; Newest threads at the top.
  (mml-enable-flowed nil)               ; Don't use f=f for now.
;;;; Cosmetic things.
  (notmuch-search-result-format
   '(("date"    . "%12s ")
     ("count"   . "%-7s ")
     ("authors" . "%-30s ")
     ("subject" . "%-70s ")
     ("tags"    . "(%s)")))
;;;; What to show on the "frontpage".
  (notmuch-show-logo nil)
  (notmuch-hello-sections '((lambda () (widget-insert "\n"))
                            notmuch-hello-insert-header
                            notmuch-hello-insert-saved-searches
                            notmuch-hello-insert-alltags))
;;;; Things my mail client announces to the world.
  (mail-host-address "hyperspace")
  (notmuch-mua-user-agent-function
   (lambda ()
     (concat "Notmuch/" (notmuch-cli-version) ", Emacs " emacs-version)))
;;;; Properly format citations.
  (message-citation-line-format   "On %a, %b %d %Y %H:%M, %N wrote:\n"     )
  (message-citation-line-function #'message-insert-formatted-citation-line )
  (notmuch-mua-cite-function      #'message-cite-original-without-signature)
;;;; Signature/Encryption settings.
  (mml-secure-openpgp-sign-with-sender t)
  (mml-secure-openpgp-encrypt-to-self  t) ; so that I can read sent encrypted mails
;;;; Attachments
  (notmuch-mua-attachment-regexp        ; never forget them again!
   "\\(attache?ment\\|attached\\|attach\\|im Anhang\\|i\\.A\\.\\|angehängt\\|hänge\\)")
  :config
  (use-package consult-notmuch)
  (use-package mailboxes   :ensure nil :demand t)
  (use-package org-notmuch :ensure nil :demand t))

(provide 'email)
