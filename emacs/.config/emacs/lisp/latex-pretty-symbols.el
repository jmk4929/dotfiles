;;; latex-pretty-symbols.el --- More `prettify-symbols-mode' symbols -*- lexical-binding: t; -*-

;;; More Pretty Symbols
;; 24dec2019 ho-ho-ho, 30may2021

(use-package emacs
  :hook
  (LaTeX-mode . prettify-symbols-mode  )
  (LaTeX-mode . slot/more-latex-symbols)
  :custom
  (prettify-symbols-unprettify-at-point 'right-edge) ; on or immediately after the symbol
  :config
  ;; This site seems to be good for symbols (use dec): https://unicodelookup.com
  (defun slot/more-latex-symbols ()
    (mapc (lambda (pair) (push pair prettify-symbols-alist))
          '(("\\nat"         . ?\u2115) ; ℕ
            ("\\real"        . ?\u211D) ; ℝ
            ("\\colon"       . ?\u003A) ; :
            ("\\from"        . ?\u003A)
            ("\\trightarrow" . 8594   )
            ("\\xrightarrow" . 8594   )
            ("\\inj"         . 8618   ) ; ↪
            ("\\emb"         . 8618   )
            ("\\sur"         . 8608   ) ; ↠
            ("\\proj"        . 8608   )
            ("\\defeq"       . 8788   ) ; ≔
            ("\\dots"        . 8230   ) ; …
            ("\\mathbb{P}"   . 8473   ) ; ℙ
            ("\\implies"     . ?\u21D2) ; ⇒
            ("\\ox"          . 8855   ) ; ⊗
            ("\\iso"         . 10610  ) ; ⥲
            ("\\mathcal{C}"  . 120018 ) ; 𝓒
            ("\\cc"          . 120018 )
            ("\\cat{C}"      . 120018 )
            ("\\cat C"       . 120018 )
            ("\\mathfrak{C}" . 120174 ) ; 𝕮
            ("\\mathfrak{D}" . 120175 ) ; 𝕯
            ("\\mathcal{D}"  . 120019 ) ; 𝓓
            ("\\dd"          . 120019 )
            ("\\cat D"       . 120019 )
            ("\\cat{D}"      . 120019 )
            ("\\mathcal{E}"  . 120020 ) ; 𝓔
            ("\\ee"          . 120020 )
            ("\\mm"          . 120028 ) ; 𝓜
            ("\\cat M"       . 120028 )
            ("\\cat{M}"      . 120028 )
            ("\\nn"          . 120029 ) ; 𝓝
            ("\\cent"        . 120041 ) ; 𝓩
            ("\\ZCat"        . 120041 )
            ("\\nt"          . 10233  ) ; ⟹
            ("\\mathbb{B}"   . 120121 ) ; 𝔹
            ("^2"            . 178    ) ; ²
            ("^l"            . ?\u02E1) ; ˡ
            ("^r"            . ?\u02B3) ; ʳ
            ("^T"            . ?\u1d40) ; ᵀ
            ("_0"            . 8320   ) ; ₀
            ("_1"            . 8321   ) ; ₁
            ("_2"            . 8322   ) ; ₂
            ("\\blank"       . 8212   ) ; —
            ("\\lact"        . 9657   ) ; ▹
            ("\\ract"        . 9667   ) ; ◃
            ("\\ref"         . ?☞     )
            ("\\eqref"       . ?☞     )
            ("\\cite"        . ?†     )
            ("\\footnote"    . ?‡     )
            ("\\label"       . ?‡     )
            ("\\tak"         . (215 (br . cl) ?A)) ; ×_A
            ("\\vartheta"    . ?ϑ     )
            ))))

(provide 'latex-pretty-symbols)
