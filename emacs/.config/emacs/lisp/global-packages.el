;;; global-packages.el --- Packages that I need globally -*- lexical-binding: t; -*-

;;; `blink-cursor-mode'
;; "Besides, your editor came to a thoroughly objective conclusion many
;; years ago that blinking cursors are an annoying distraction and that
;; any developer implementing such behavior should be sentenced to ten
;; years of COBOL coding under a strobe light."
(blink-cursor-mode 0)

;;; `global-subword-mode'
;; Treat camelCase and PascalCase words like kebab-case and snake_case
;; with regards to word operations.
(global-subword-mode)

;;; `pixel-scroll-precision-mode'
;; Scroll the display pixel-wise; needs XInput 2.4 (and hence
;; `xorg-server' 1.21+).
(use-package pixel-scroll-precision-mode
  :ensure nil
  :init (pixel-scroll-precision-mode)
  :custom (pixel-scroll-precision-interpolate-page t))

;;; We like to know where we've been.
(save-place-mode)                         ; place in file
(use-package minibuffer                   ; minibuffer history
  :ensure nil
  :hook (minibuffer-mode . savehist-mode)
  :custom (history-delete-duplicates t))
(use-package recentf                      ; open files
  :demand t
  :custom (recentf-max-saved-items 50)
  :config (recentf-mode))

;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
;; Repeat stuff, repeat stuff, repeat stuff!
(repeat-mode)

;;; `hl-line'
(global-hl-line-mode)

;;; `auto-revert'
;; When something changes in a file, automatically refresh the buffer.
(global-auto-revert-mode)
(setq auto-revert-use-notify nil)

;;; `xclip'
;; Make terminal Emacs aware of the system clipboard without much
;; fussing about.
(use-package xclip                      ; 24nov2020
  :demand t
  :config (xclip-mode))

;;; `ix.io'
;; Pasting stuff to ix.io.
(use-package ix.io                      ; 20nov2020
  :vc (:fetcher "gitlab" :repo "slotThe/ix.io")
  :commands ix.io-paste-region)

;;; `olivetti'
;; 13oct2020
(use-package olivetti
  :hook ((notmuch-show-mode
          notmuch-message-mode
          notmuch-hello-mode
          notmuch-search-mode
          org-present-mode)
         . olivetti-mode)
  :custom (olivetti-body-width 0.8))

;;; `expand-region'
;; 15oct2020 This is useful sometimes \o\
(use-package expand-region
  :bind ("C-M-SPC" . 'er/expand-region)
  :config
  (defun er/mark-comment () ; 30oct2022 https://github.com/karthink/.emacs.d/blob/e0dd53000e61936a3e9061652e428044b9138c8c/init.el#L2413
    "Mark the entire comment around point.
The default er/mark-comment is both expensive and incorrect for
block comments."
    (interactive)
    (when (er--point-is-in-comment-p)
      (let ((p (point)))
        (while (and (er--point-is-in-comment-p) (not (eobp)))
          (forward-word 1))
        (while (not (or (er--point-is-in-comment-p) (bobp)))
          (forward-char -1))
        (set-mark (point))
        (goto-char p)
        (while (er--point-is-in-comment-p)
          (forward-word -1))
        (while (not (or (er--point-is-in-comment-p) (eobp)))
          (forward-char 1))))))

;;; `yasnippet'
;; 07dec2020, 20nov2021
;; Who would want to live without snippets?
(use-package yasnippet
  :demand t
  :hook (yas-minor-mode . (lambda () (yas-activate-extra-mode 'fundamental-mode)))
  :config (yas-global-mode)
  :custom
  (yas-inhibit-overlay-modification-protection nil)
  (yas-snippet-dirs `(,(concat user-emacs-directory "snippets"))))

;;; `avy'
;; 18jan2021
;; Move through the visible part of the buffer with your mind!
(use-package avy
  :bind (("C-." . avy-goto-char-timer)
         :map isearch-mode-map
         ("C-." . avy-isearch))
  :custom
  (avy-timeout-seconds 0.2)
  ;; Keys that fit my keyboard layout
  (avy-keys '(?t ?n ?s ?e     ; home row index and middle finger, alternating
                 ?d ?h        ; index finger curl
                 ?f ?u        ; middle finger stretch
                 ?r ?i ?a ?o  ; rest of home-row, alternating
                 ?w ?y)))     ; ring finger stretch

;;; `hl-todo'
;; 09dec2021 add PONDER
(use-package hl-todo
  :hook ((prog-mode LaTeX-mode markdown-mode org-mode) . hl-todo-mode)
  :config (add-to-list 'hl-todo-keyword-faces
                       `("PONDER" . ,(cdr (assoc "FIXME" hl-todo-keyword-faces)))))

;;; `helpful'
;; 09jan2019
;; `Helpful' is a package that makes the default help pages a bit more
;; readable (some syntax highlighting, as well as trying to get the
;; source code if possible).
(use-package helpful
  :bind (("C-c C-d"                 . #'helpful-at-point)
         ([remap describe-function] . #'helpful-callable)
         ([remap describe-variable] . #'helpful-variable)
         ([remap describe-key     ] . #'helpful-key     ))
  :custom
  (helpful-switch-buffer-function       ; re-use the same buffer when going deeper
   (lambda (buffer-or-name)
     (if (eq major-mode 'helpful-mode)
         (popwin:switch-to-buffer buffer-or-name)
       (popwin:pop-to-buffer buffer-or-name)))))

;;; `magit'
;; 07dec2019 add forges, 08oct2020 libgit
;; I use `magit' to handle version control like a civilized person.
(use-package magit
  :hook ((magit-status-mode magit-diff-mode) . visual-line-mode)
  :custom
  (git-commit-summary-max-length 50)
  (magit-diff-refine-hunk 'all)       ; 27mar2023 replaces `magit-delta'
  :config
  (use-package forge)
  (use-package magit-section)
  (use-package magit-todos              ; 20feb2023
    :hook (magit-status-mode . magit-todos-mode)))

;;; `diff-hl'
(use-package diff-hl                    ; 26dec2021
  :hook ((prog-mode LaTeX-mode) . turn-on-diff-hl-mode)
  :custom (diff-hl-draw-borders nil))

;;; `project.el'
  :bind (("C-x C-f" . project-find-file))
(use-package project                    ; 12jan2021
  :custom (project-switch-commands
           '((magit-project-status "Magit"     ?m)
             (project-find-file    "Find file" ?f)
             (project-dired        "Dired"     ?d)
             (project-eshell       "Eshell"    ?e))))

;;; `dictcc'
(use-package dictcc
  :bind ("<f2>" . dictcc)
  :custom (dictcc-completion-backend 'completing-read))

;;; `undo-tree'
;; I would very much like an undo tree, thank you.
(use-package undo-tree              ; 09jan2019, 30aug2020
  :demand t
  :bind (("M-u" . undo-tree-undo)
         ("M-U" . undo-tree-redo))
  :config (global-undo-tree-mode)
  :custom
  ;; Causes problem with at least `elfeed'; possibly others as well.
  (undo-tree-auto-save-history nil)
  ;; This setting is apparently really buggy, so better disable it.
  (undo-tree-enable-undo-in-region nil))

;;; `multiple-cursors'
(use-package multiple-cursors           ; 07sep2021
  :bind (("C-M->" . mc/mark-next-lines    )
         ("C-M-<" . mc/mark-previous-lines))
  :preface
  (unbind-key (kbd "C-M-."))
  (unbind-key (kbd "C-M-,"))
  (defrepeatmap mc-repeat-map           ; 26aug2022
    '(("M-s s" . mc/mark-next-like-this-word)
      ("C-M-." . mc/mark-next-word-like-this)
      ("C-M-," . mc/mark-previous-word-like-this)
      ("C->"   . mc/skip-to-next-like-this)
      ("C-<"   . mc/skip-to-previous-like-this))
    "`repeat-mode' keymap to repeat `multiple-cursors' bindings."))

;;; `emacs-anywhere'
(use-package emacs-anywhere             ; 20sep2021
  :vc (:fetcher "gitlab" :repo "slotThe/emacs-anywhere")
  :commands emacs-anywhere)

;;; `ledger-mode'
(use-package ledger-mode                ; 02nov2021
  :custom
  (ledger-reconcile-default-commodity "EUR")
  ;; for hledger
  ;; (ledger-mode-should-check-version nil)
  ;; (ledger-binary-path (executable-find "hledger"))
  ;; (ledger-report-links-in-register nil)
  ;; (ledger-report-native-highlighting-arguments '("--color=always"))
  ;; (ledger-report-auto-width nil)
  :config
  (add-to-list 'auto-mode-alist '("\\.journal\\'" . ledger-mode)))

;;; `exercism'
(use-package exercism                   ; 02nov2021
  :vc (:fetcher "gitlab" :repo "slotThe/exercism")
  :commands exercism-new)

;;; `hide-mode-line-mode'
;; 02sep2022
(use-package hide-mode-line)

;;; TRAMP
;; 08jan2023
(use-package tramp
  :custom
  (vc-handled-backends '(Git))
  (remote-file-name-inhibit-locks t))

;;; `beframe'
(use-package beframe                    ; 05mar2023
  :vc (:fetcher github :repo "protesilaos/beframe")
  :after consult
  :demand t
  :custom (beframe-global-buffers '("*scratch*" "*Messages*"))
  :config
  (defvar beframe--consult-source
    `(:name     "Frame-specific buffers (current frame)"
      :category buffer
      :history  beframe-history
      :items    (lambda (&optional frame)
                  (beframe-buffer-names frame :sort #'beframe-buffer-sort-visibility))
      :action   ,#'switch-to-buffer
      :state    ,#'consult--buffer-state))
  (add-to-list 'consult-buffer-sources 'beframe--consult-source)
  ;; Make all buffers part of the history, but put them at the very end.
  (setq consult-buffer-sources (remove 'consult--source-buffer consult-buffer-sources))
  (add-to-list 'consult-buffer-sources 'consult--source-buffer 'append))

(provide 'global-packages)
