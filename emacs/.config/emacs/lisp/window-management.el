;;; window-management.el --- Keep windows out of my way -*- lexical-binding: t; -*-

;; Set the frame title for emacs in a possible status bar.
;; This is important for programs like `arbtt'.
(use-package emacs
  :hook                                 ; 26mar2022
  (eshell-mode . (lambda ()
                   (setq-local frame-title-format
                               '("eshell in " eshell-directory-name))))
  (magit-mode . (lambda ()
                  (setq-local frame-title-format
                              '("magit in " magit--default-directory))))
  :custom (frame-title-format "%f"))

;; Make Emacs split windows in a more sane way (proportionally).
(setq window-combination-resize t)

;;; `popwin'
;; 24dec2019 ho-ho-ho, 20oct2020 cleanup for `frames-only-mode', 19nov2020, 21nov2020

;; Make some windows into popup windows, so they don't disturb the flow
;; that much.
(use-package popwin
  :demand t
  :config (popwin-mode)
  :bind (("C-c P" . popwin:popup-last-buffer)) ; Open the last buffer again.
  :config
  (mapc (lambda (x) (push x popwin:special-display-config))
        '((" *CDLaTeX Help*")
          (helpful-mode)
          ("*Cargo Run*" :noselect t)
          ("*hoogle*" :noselect t)
          ("*Messages*" :noselect t :height 0.2)
          ("*Org Agenda*" :height 0.5)
          (comint-mode :height 0.5)         ; Editable `compile-mode'
          (magit-revision-mode :height 0.5) ; Details of a commit in magit
          (" *undo-tree*" :width 0.2 :position left)
          ("*Synonyms List*" :height 0.4)
          ("*elfeed-entry*" :height 0.8 :position bottom)
          ("*eldoc*" :height 0.4 :noselect t)
          )))

;;; Window Repeat Map
(defrepeatmap window-repeat-map         ; 21sep2021
  '(("^" . enlarge-window             )
    ("}" . enlarge-window-horizontally)
    ("{" . shrink-window-horizontally )
    ("+" . balance-windows            )
    ("o" . other-window               )
    ("1" . delete-other-windows       )
    ("2" . split-window-below         )
    ("3" . split-window-right         )
    ("0" . delete-window              ))
  "`repeat-mode' keymap to repeat window key sequences.")

(provide 'window-management)
