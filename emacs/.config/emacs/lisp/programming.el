;;; programming.el --- Small programming configurations -*- lexical-binding: t; -*-

;;; General Programming
(use-package prog-mode
  :ensure nil
  :bind (:map prog-mode-map
              ([f5] . compile))
  :custom
  (compilation-scroll-output 'first-error) ; Scroll to first error
  (compile-command "stack build")          ; Default command
  :config
  (global-font-lock-mode)
  (setq-default   ; Always indent with spaces, never use tabs; #FuckTabs
   indent-tabs-mode  nil
   default-tab-width 2
   standard-indent   2)
  ;; 28dec2022 for void templates
  (add-to-list 'auto-mode-alist '("template" . conf-mode)))

;;; `flycheck'
;; Most IDE-like packages automatically set hooks for `flycheck', so
;; this configuration is rather minimal.
(use-package flycheck
  :hook ((markdown-mode message-mode latex-mode) . flycheck-mode) ; `proselint'
  :bind (("M-n" . flycheck-next-error)
         ("M-p" . flycheck-previous-error)))

;;; Haskell \o/
;; 23mar2020, 08oct2020 nav imports bidirectionally, 22sep2021
(use-package haskell-mode
  :preface
  (defun slot/haskell-load-and-bring ()
    "Sane behaviour when loading the current file into ghci."
    (interactive)
    (save-buffer)
    (haskell-process-load-file)
    (haskell-interactive-bring))

  (defun slot/haskell-add-pragma ()
    "Add a pragma to the function above point."
    (interactive)
    (let* ((fun-name (save-excursion
                       (re-search-backward "^[\s]*[[:ascii:]]* ::"
                                           (save-excursion (previous-line 50) (point)))
                       (progn (back-to-indentation)
                              (buffer-substring (point)
                                                (1- (search-forward " " (point-at-eol)))))))
           (choice (completing-read (concat "Add to `" fun-name "': ")
                                    '("INLINE" "INLINABLE" "SPECIALISE"))))
      (insert "{-# " choice " " fun-name " #-}")))

  :bind (:map haskell-mode-map
              ("C-c M-." . hoogle                         )
              ("C-c C-p" . slot/haskell-add-pragma        )
              ;; Jump to the import blocks and back in current file.
              ([f12]     . haskell-navigate-imports       )
              ([f11]     . haskell-navigate-imports-return)
              ;; Interactive stuff
              ("C-c C-c" . slot/haskell-load-and-bring    )
              ("C-c C-z" . haskell-interactive-switch     )
              ;; For the times when the LSP stuff fails
              ("C-c ."   . haskell-process-do-type        )
              ("C-c ,"   . haskell-process-do-info        )
              ("C-M-;"   . haskell-mode-jump-to-def-or-tag)
              :map puni-mode-map
              ("C-d"     . delete-char                    )
              ("DEL"     . delete-backward-char           ))
  :custom
  (haskell-interactive-popup-errors nil) ; Don't pop up errors in a separate buffer.
  (haskell-process-type 'stack-ghci)
  (haskell-process-path-ghci "stack")
  (haskell-indentation-where-pre-offset  1)
  (haskell-indentation-where-post-offset 1)
  (haskell-process-auto-import-loaded-modules t))

(use-package hindent
  :hook ((haskell-mode lsp-mode) . hindent-mode)
  :custom
  (hindent-process-path "fourmolu")
  (hindent-extra-args '("-o" "-XBangPatterns"
                        "-o" "-XTypeApplications"
                        "--indentation" "2")))

;; Yes, this is a mode for a config file I will probably edit for a
;; grand total of 5 minutes ever.  Fight me.
(use-package ghci-conf-mode             ; 28nov2020
  :ensure nil)

;;; Shell
(use-package emacs
  :hook
  (sh-mode . flycheck-mode)             ; Uses shell-check
  ;; When saving a file that starts with "#!", make it executable.
  (after-save . executable-make-buffer-file-executable-if-script-p))

;;; Agda --- `agda2-mode' is a masterpiece.
;; 20sep2020, 17sep2021
(load-file (let ((coding-system-for-read 'utf-8))
             (shell-command-to-string "agda-mode locate")))
;; Lazy load `agda2-mode', yet have the "Agda" input method available
;; for C-\ all the time.
(advice-add 'toggle-input-method :before
            (lambda () (interactive) (require 'agda2-mode)))

;;; Rust
;; Rust is like Haskell but without the cool functional-ness!
(use-package rustic ; 05feb2020, 12feb2022, 28aug2022 rust-mode -> rustic-mode
  :bind (:map rustic-mode-map
              ("C-c m" . lsp-rust-analyzer-expand-macro)))

;;; Clojure
;; Clojure is like Haskell but without the cool type system!  If you say
;; homoiconic in front of your bathroom mirror three times with the
;; lights out, the ghost of John McCarthy appears and hands you a
;; parenthesis.
(use-package cider
  :hook (((cider-mode cider-repl-mode) . cider-company-enable-fuzzy-completion)
         (cider-mode . (lambda ()
                         (require 'flycheck-clj-kondo)
                         (flycheck-mode))))
  :bind (:map cider-mode-map
              ("C-c C-v C-e" . cider-eval-buffer)
              ("C-c C-v M-;" . cider-pprint-eval-last-sexp-to-comment))
  :custom (cider-enrich-classpath t)
  :config (use-package flycheck-clj-kondo))

;;; Emacs Lisp
(use-package emacs                      ; 15jan2023, 12mar2023
  :bind (:map emacs-lisp-mode-map
              ("C-c <return>"   . pp-macroexpand-last-sexp)
              ("C-c C-<return>" . emacs-lisp-macroexpand)))

;;; Nixlang
(use-package nix-mode
  :when (string= system-name desktop-name))

;;; Markdown
(use-package markdown-mode
  ;; Associate .md files with github-flavoured markdown (this is
  ;; _really_ sad, but I guess that's what we have to deal with when it
  ;; comes to monopolies).
  :mode (("\\.md$" . gfm-mode))
  :commands gfm-mode
  :bind (:map markdown-mode-map ("C-c l" . slot/often-used-links))
  :custom (markdown-command "pandoc --standalone --mathjax --from=markdown"))

(defun slot/often-used-links (&optional arg)
  "Choose a link and insert it into the buffer in .md format.
This is quite useful, since many people happen to have very
similar problems when, for example, first starting out with
xmonad."
  (interactive "P")
  (cl-flet
      ((get-xmonad-modules ()
         "Get all XMonad modules in the form (NAME . DOC-URL)."
         (let* ((xmonad-cabal "~/repos/xmonad/xmonad-contrib/xmonad-contrib.cabal")
                (modules (shell-command-to-string
                          (format "tail -n +50 %s | grep -E \" XMonad\\.*\""
                                  xmonad-cabal))))
           (->> (s-lines modules)
                (-drop-last 1)          ; empty line
                (--map (s-trim (s-replace "exposed-modules:" "" it)))
                (--map (cons it
                             (format "https://hackage.haskell.org/package/xmonad-contrib/docs/%s.html"
                                     (s-replace "." "-" it)))))))
       (get-posts ()
         "Get all of my blog posts in the form (NAME . URL)."
         (let* ((website "https://tony-zorman.com/")
                (base-path "~/repos/slotThe.github.io/")
                (posts (directory-files-recursively (concat base-path "posts/") ".md$")))
           (--map (with-temp-buffer
                    (insert-file-contents-literally it)
                    (search-forward "title: ")
                    (cons               ; Name . URL
                     (string-replace "\"" "" (buffer-substring (point) (point-at-eol)))
                     (concat website (string-trim it base-path ".md") ".html")))
                  posts))))
    (-let* ((links
             (-concat '(("tutorial" . "https://xmonad.org/TUTORIAL.html")
                        ("install"  . "https://xmonad.org/INSTALL.html")
                        ("xmonad.hs". "https://gitlab.com/slotThe/dotfiles/-/blob/master/xmonad/.config/xmonad/src/xmonad.hs"))
                      (get-xmonad-modules)
                      (get-posts)))
            (choice (completing-read "Link: " (mapcar #'car links)))
            ((name . link) (assoc choice links)))
      (insert "[" name "]")
      (if arg
          (insert "(" link ")")
        (save-excursion (insert "\n\n[" name "]: " link))))))

;;; `kbd-mode'
(use-package kbd-mode ; 24sep2020 https://github.com/kmonad/kbd-mode
  :vc (:fetcher "github" :repo "kmonad/kbd-mode")
  :mode "\\.kbd\\'"
  :commands kbd-mode
  :custom
  (kbd-mode-kill-kmonad "pkill -9 kmonad")
  (kbd-mode-start-kmonad "kmonad ~/.config/kmonad/x220-slot-us-colemak-dh-z.kbd"))

;;; `lsp-mode'
;; I really don't like LSP solutions, but some languages seem to require
;; it...
(use-package lsp-mode ; 05feb2020, 23mar2020, 28oct2021, 12dec2021
  :preface
  (defun slot/lsp-shutdown-last-workspaces ()
    "Shut down the `lsp--last-active-workspaces'."
    (interactive)
    (mapc (lambda (ws)
            (lsp--warn "Stopping %s" (lsp--workspace-print ws))
            (with-lsp-workspace ws (lsp--shutdown-workspace)))
          lsp--last-active-workspaces))
  :hook
  ((haskell-mode rust-mode LaTeX-mode) . lsp-deferred)
  (lsp-mode . (lambda ()
                (setq-local read-process-output-max (* 1024 1024))))
  (lsp-completion-mode                  ; for corfu compatibility
   . (lambda ()
       (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
             '(flex))))
  :commands (lsp-deferred lsp)
  :bind (:map lsp-mode-map
              ("C-c l i" . lsp-ui-imenu)
              ("C-c C-r" . lsp-execute-code-action)
              ("C-c C-d" . lsp-ui-doc-show)
              ("C-c l s" . (lambda ()
                             (interactive)
                             (lsp-ui-sideline-enable (not lsp-ui-sideline-mode))))
              ("C-c h"   . (lambda ()
                             (interactive)
                             (when lsp-lens-mode
                               (lsp-lens-mode -1))
                             (lsp-lens-hide)))
              ("C-c d"   . (lambda ()
                             (interactive)
                             (when (not lsp-lens-mode)
                               (lsp-lens-mode 1))
                             (lsp-lens-show))))
  :custom
  (lsp-keep-workspace-alive nil)
  (lsp-use-plists t)
  (lsp-completion-provider :none)       ; Corfu
  (lsp-keymap-prefix "C-c l")
  (lsp-idle-delay 0.6)
  (lsp-signature-render-documentation nil) ; Rust sends _way too much_ here
  ;; Disable things I don't care about
  (lsp-headerline-breadcrumb-enable nil)
  (lsp-modeline-code-actions-enable nil)
  (lsp-modeline-diagnostics-enable nil)
  (lsp-enable-symbol-highlighting nil)
  (lsp-enable-text-document-color nil)
  (lsp-enable-folding nil)
  (lsp-enable-on-type-formatting nil))

(use-package lsp-ui
  :after lsp-mode
  :commands lsp-ui-mode
  :custom
  (lsp-ui-doc-max-width 80)
  (lsp-ui-doc-show-with-mouse nil)
  (lsp-ui-sideline-enable nil)
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-sideline-ignore-duplicate t)
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t))

(defun slot/lsp-get-type-signature (lang str)
  "Get LANGs type signature in STR.
Original implementation from https://github.com/emacs-lsp/lsp-mode/pull/1740."
  (let* ((start (concat "```" lang))
         (groups (--filter (s-equals? start (car it))
                           (-partition-by #'s-blank? (s-lines (s-trim str)))))
         (name-at-point (symbol-name (symbol-at-point)))
         (type-sig-group (car
                          (--filter (s-contains? name-at-point (cadr it))
                                    groups))))
    (->> (or type-sig-group (car groups))
         (-drop 1)                      ; ``` LANG
         (-drop-last 1)                 ; ```
         (-map #'s-trim)
         (s-join " "))))

;;;; Language specific LSP things

;;;;; Haskell

(use-package lsp-haskell
  :after lsp-mode
  :preface
  (defun slot/lsp-haskell-type-signature ()
    "Add a type signature for the thing at point.
This is very convenient, for example, when dealing with local
functions, since those—as opposed to top-level expressions—don't
have a code lens for \"add type signature here\" associated with
them."
    (interactive)
    (let* ((value (-some->> (lsp--text-document-position-params)
                    (lsp--make-request "textDocument/hover")
                    lsp--send-request
                    lsp:hover-contents
                    (funcall (-flip #'plist-get) :value))))
      (slot/back-to-indentation)
      (insert (slot/lsp-get-type-signature "haskell" value))
      (haskell-indentation-newline-and-indent)))

  ;; Fixes https://github.com/emacs-lsp/lsp-haskell/issues/151
  (cl-defmethod lsp-clients-extract-signature-on-hover
    (contents (_server-id (eql lsp-haskell)))
    "Display the type signature of the function under point."
    (message "%s" "lsp-haskell")
    (let* ((sig (slot/lsp-get-type-signature "haskell" (plist-get contents :value))))
      (lsp--render-element (concat "```haskell\n" sig "\n```"))))

  :bind (:map lsp-mode-map
              ("C-c C-t" . slot/lsp-haskell-type-signature))
  :config (add-hook 'lsp-lsp-haskell-after-open-hook
                    (lambda ()
                      (setq-local lsp-signature-render-documentation t))
                    nil t))

;;;;; Rust

(use-package emacs
  :after (rust-mode lsp-mode)
  :preface
  (cl-defmethod lsp-clients-extract-signature-on-hover
    (contents (_server-id (eql rust-analyzer)))
    "Display the type signature of the function under point."
    (let* ((sig (slot/lsp-get-type-signature "rust" (plist-get contents :value))))
      (lsp--render-element (concat "```rust\n" sig "\n```"))))
  :custom            ; 28aug2022
  ;; (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-closure-return-type-hints t))

;;; `paredit'
(use-package paredit                 ; 04sep2022 lispy -> paredit
  :hook ((emacs-lisp-mode clojure-mode) . enable-paredit-mode)
  :bind (:map paredit-mode-map
              ("M-s"     . nil)
              ("C-x C-d" . paredit-splice-sexp)))

(provide 'programming)
