#! /usr/bin/env python
from subprocess import run

def get(s: str) -> str:
    return run(
        "gpg -dq --for-your-eyes-only --no-tty " + s,
        shell=True,
        capture_output=True,
        text=True,
    ).stdout.strip("\n")

def get_pass() -> str:
    return get("~/.config/offlineimap/uni.gpg")

def get_pass2() -> str:
    return get("~/.config/offlineimap/mailbox.gpg")

def get_pass3() -> str:
    return get("~/.config/offlineimap/mailbox-slot.gpg")
