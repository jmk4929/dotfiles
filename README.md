# My dotfiles

What could you possibly want to know about a random dotfiles repo. Feel
free to use this stuff for anything you want, respecting the LICENSE of
course ":D".

These dotfiles are set up in a way such that they can be cloned directly
into a `.dotfiles` directory in `$HOME` and symlinked in batches with
[GNU Stow].

In addition to just dotfiles there are also lots of handy little scripts
strewn around.  HFHF

[GNU Stow]: https://www.gnu.org/software/stow/

## Specs

  - **OS**: [Void Linux](https://voidlinux.org/)
  - **WM**: [xmonad](https://xmonad.org/)
  - **Bar**: [xmobar](https://github.com/jaor/xmobar)
  - **Editor**: [GNU Emacs](https://www.gnu.org/software/emacs/)
  - **Terminal**: [alacritty](https://github.com/alacritty/alacritty)
  - **Launcher**: [hmenu](https://gitlab.com/slotThe/hmenu) (wrapper
    around [dmenu](https://tools.suckless.org/dmenu/))

## READMEs for individual programs

The configuration for some programs is so extensive that an individual
README makes sense for them. Much better than trying to cam it all into
this document!

  - [XMonad](xmonad/.config/xmonad/README.md)
  - [Emacs](emacs/.config/emacs/README.md)

## Other stuff that I use so you should at least look at this list

Things that you are probably going to need (I'm most likely forgetting
something here). Bigger config files (like my Emacs configuration) also
have a list of the specific third-party programs they use at the top.

| Tool                                                                 | Usage                                                 |
|----------------------------------------------------------------------|-------------------------------------------------------|
| [atool (aunpack)](https://www.nongnu.org/atool/)                     | automatic unpacking                                   |
| [doas](https://github.com/slicer69/doas)                             | replacement for `sudo`                                |
| [exa](https://the.exa.website/)                                      | replacement for `ls`                                  |
| [fd](https://github.com/sharkdp/fd)                                  | replacemnt for `find`                                 |
| [feh](https://feh.finalrewind.org/)                                  | set bg that you'll never see                          |
| [zsh](https://www.zsh.org/)                                          | Shell                                                 |
| [fzf](https://github.com/junegunn/fzf)                               | fuzzy finding things! (duh)                           |
| [GHC](https://www.haskell.org/ghc/)                                  | compile category theory                               |
| [GNU Stow](https://www.gnu.org/software/stow/)                       | dotfiles management                                   |
| [slock](https://tools.suckless.org/slock/)                           | lock the screen                                       |
| [KMonad](https://github.com/david-janssen/kmonad/)                   | all things keyboard layout                            |
| [mpv](https://mpv.io/)                                               | video player                                          |
| [Redshift](https://github.com/jonls/redshift/)                       | pee on screen at night                                |
| [ripgrep](https://github.com/BurntSushi/ripgrep)                     | replacement for `grep`, `ag`, `ack`, ...              |
| [sxiv](https://github.com/muennich/sxiv)                             | image viewer (can play `.gifs` \o/)                   |
| [unclutter-xfixes](https://github.com/Airblader/unclutter-xfixes/)   | hide cursor when idle                                 |
| [xfce4-power-manager](https://git.xfce.org/xfce/xfce4-power-manager) | power manager for laptop                              |
| [xdotool](https://github.com/jordansissel/xdotool)                   | Fake Keyboard/Mouse Input                             |
| [youtube-dl](https://ytdl-org.github.io/youtube-dl/index.html)       | download videos from a lot of sites                   |
| [zathura](https://git.pwmt.org/pwmt/zathura)                         | PDF/PS viewer                                         |
| [vdirsyncer](https://github.com/pimutils/vdirsyncer)                 | For syncing nextcloud stuff                           |
| [khal](https://github.com/pimutils/khal)                             | For accessing the synced nextcloud stuff (via khalel) |
| davfs2                                                               | WEBDAV nextcloud stuff                                |

## Unmaintained Configs

The following fell into disuse and are only here for sentimental
purposes.

  - [(neo)vim](https://neovim.io) — replaced by the one and only GNU
    Emacs

## Removed Configs

Go dig in the `git` history if you want :)

  - [(neo)mutt](https://neomutt.org/) — replaced by `notmuch.el`
