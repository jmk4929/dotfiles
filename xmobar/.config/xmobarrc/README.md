# Xmobar Configuration

My personal—Haskell based—xmobar configuration.

# Building

Simply build with `stack install` (or `stack build` and then copy the
executables). Xmobar will already be compiled with the right
configuration, so just invoking `xmobar` will work.
