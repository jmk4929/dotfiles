" Runtimepath settings
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

let g:loaded_python_provider = 1
let g:python3_host_skip_check=1
let g:python3_host_prog = '/usr/bin/python3'

" Source vimrc
source ~/.vimrc
