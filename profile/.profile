#!/bin/sh

### This file is run on login.

# Set XDG variables
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

# Force certain paths into $XDG_CONFIG_HOME
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch-config"
export XMONAD_CACHE_DIR="$XDG_CONFIG_HOME/xmonad"
export XMONAD_CONFIG_DIR="$XDG_CONFIG_HOME/xmonad"
export XMONAD_DATA_DIR="$XDG_CONFIG_HOME/xmonad"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export CARGO_HOME="$XDG_CONFIG_HOME/cargo"
export RUSTUP_HOME="$XDG_CONFIG_HOME/rustup"
export PASSWORD_STORE_DIR="$XDG_CONFIG_HOME/password-store"
export LEDGER_FILE="$XDG_CONFIG_HOME/hledger/journal.ledger"

# Default applications.
export EDITOR="emacsclient -s termEmacs -a \"\" --tty"
export BROWSER="firefox"
export TERMINAL="alacritty"

# Lol
export LSP_USE_PLISTS=true

# Use dmenu as a password handler.
export SUDO_ASKPASS="$HOME/.scripts/dmenupassword"

# `xtools' needs this for some things
export XBPS_DISTDIR="$HOME/repos/void-packages/"

# Explicitly export LC_ALL (due to musl).
export LC_ALL="en_US.UTF-8"

# Add things to the path.
export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin:$HOME/.local/bin:/opt/texlive/2023/bin/x86_64-linuxmusl:$HOME/.cabal/bin:$HOME/.scripts:$CARGO_HOME/bin

# Colours for less/man.
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"
